	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php if ($modulo = get_theme_mod('ggl_modulo_footer')) {
    echo do_shortcode('[et_pb_section global_module="'.$modulo.'"][/et_pb_section]');
} ?>

		</div> <!-- #et-main-area -->

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>
</html>
