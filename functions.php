<?php
//Fuerza a cargar las hojas de estilo cada vez que se cambie un número
define('GGL_VERSION', "1.4.4");

/* include custom image module file*/
include(get_stylesheet_directory() . '/ggl-taxonomy.php');
//include(get_stylesheet_directory() . '/modulos-divi-gg.php');
include(get_stylesheet_directory() . '/divi/modulos/autor.php');
include(get_stylesheet_directory() . '/divi/modulos/blog-nodo.php');
include(get_stylesheet_directory() . '/divi/modulos/eventos.php');
include(get_stylesheet_directory() . '/divi/modulos/historias.php');
include(get_stylesheet_directory() . '/divi/modulos/loop.php');

add_action('wp_enqueue_scripts', 'ggl_theme_enqueue_styles');
add_action('customize_register', 'ggl_customize_options');

add_filter('body_class', 'ggl_class_sin_eventos');

add_filter( 'tribe_events_admin_show_cost_field', 'ggl_show_cost', 2, 100 );

add_action( 'init', 'ggl_remove_costs_filter', 11 );

//Función para eliminar los filtros de costos que no necesitamos.
function ggl_remove_costs_filter() {
    remove_all_filters('tribe_events_event_costs',10);
}

 function ggl_show_cost($show_cost, $modules) {
   return true;
 }

//Función para indicar en el body del frontpag/home que no hay eventos.
function ggl_class_sin_eventos($clases)
{
    $events = tribe_get_events(array('start_date' => date('Y-m-d H:i:s')));
    if (count($events) == 0) {
        $clases[] = 'no-eventos-futuros';
    }
    return $clases;
}

function ggl_theme_enqueue_styles()
{
    wp_enqueue_style('fonts', get_bloginfo('stylesheet_directory') . '/fonts/fonts.css', array(), GGL_VERSION);
    wp_enqueue_style('responsive', get_bloginfo('stylesheet_directory') . '/responsive.css', array(), GGL_VERSION);
    wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css', array('fonts', 'responsive'), GGL_VERSION);
    wp_enqueue_style('responsive', get_bloginfo('stylesheet_directory') . '/style/theme-my-login.css', array(), GGL_VERSION);
    wp_register_style('leafletjs', 'http://cdn.leafletjs.com/leaflet/v1.0.3/leaflet.css', array(), GGL_VERSION);

    wp_register_script('leafletjs', "http://cdn.leafletjs.com/leaflet/v1.0.3/leaflet.js", array('jquery'), GGL_VERSION);
    wp_register_script('leafmap', get_bloginfo('stylesheet_directory')."/scripts/leaf-map.js", array('leafletjs'), GGL_VERSION);
}

function ggl_customize_options($wp_customize)
{

    //Declarar clase para listado de módulo de divi
    require_once 'inc/divi-module-dropdown-custom-control.php';

    //Sección de Pie de página
    $wp_customize->add_section(
        'ggl_modules',
        array(
            'title' => 'Layouts',
        )
    );

    //Opción para módulo para ser usado en entrada
    $wp_customize->add_setting(
        'ggl_modulo_post',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de entrada
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_post',
            array(
                'label' => 'Entrada',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_post',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo para ser usado en página
    $wp_customize->add_setting(
        'ggl_modulo_page',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de página
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_page',
            array(
                'label' => 'Página',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_page',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo para ser usado en historia
    $wp_customize->add_setting(
        'ggl_modulo_historia',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de historia
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_historia',
            array(
                'label' => 'Historia',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_historia',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo para ser usado en página de autor
    $wp_customize->add_setting(
        'ggl_author_page',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de página de autor
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_author_page',
            array(
                'label' => 'Perfil público',
                'section' => 'ggl_modules',
                'settings' => 'ggl_author_page',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo para ser usado en search/archive
    $wp_customize->add_setting(
        'ggl_modulo_archive',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de search/archive
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_archive',
            array(
                'label' => 'Resultados (archive)',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_archive',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo para ser usado en search/archive sin resultados
    $wp_customize->add_setting(
        'ggl_modulo_archive_sinresultados',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo de search/archive sin resultados
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_archive_sinresultados',
            array(
                'label' => 'Sin resultados (archive)',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_archive_sinresultados',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo del error 404
    $wp_customize->add_setting(
        'ggl_error_404',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo al pie de página
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_error_404',
            array(
                'label' => 'Error 404',
                'section' => 'ggl_modules',
                'settings' => 'ggl_error_404',
                //'post_type' => 'et_pb_layout'
            )
        )
    );

    //Opción para módulo del pie de página
    $wp_customize->add_setting(
        'ggl_modulo_footer',
        array(
            'type' => 'theme_mod', // or 'theme_mod'
        )
    );

    //Asignar control de módulo al pie de página
    $wp_customize->add_control(
        new Divi_Module_Dropdown_Custom_Control(
            $wp_customize,
            'ggl_modulo_footer',
            array(
                'label' => 'Pie de página',
                'section' => 'ggl_modules',
                'settings' => 'ggl_modulo_footer',
                //'post_type' => 'et_pb_layout'
            )
        )
    );
}

/*this function allows for the auto-creation of post excerpts*/
if (! function_exists('truncate_post')) {
    function truncate_post($amount, $echo = true, $post = '', $strip_shortcodes = false)
    {
        global $shortname;

        if ('' == $post) {
            global $post;
        }

        $amount = 120;

        $post_excerpt = '';
        $post_excerpt = apply_filters('the_excerpt', $post->post_excerpt);

        if ('on' == et_get_option($shortname . '_use_excerpt') && '' != $post_excerpt) {
            if ($echo) {
                echo $post_excerpt;
            } else {
                return $post_excerpt;
            }
        } else {
            // get the post content
            $truncate = $post->post_content;

            // remove caption shortcode from the post content
            $truncate = preg_replace('@\[caption[^\]]*?\].*?\[\/caption]@si', '', $truncate);

            // remove post nav shortcode from the post content
            $truncate = preg_replace('@\[et_pb_post_nav[^\]]*?\].*?\[\/et_pb_post_nav]@si', '', $truncate);

            // Remove audio shortcode from post content to prevent unwanted audio file on the excerpt
            // due to unparsed audio shortcode
            $truncate = preg_replace('@\[audio[^\]]*?\].*?\[\/audio]@si', '', $truncate);

            // Remove embed shortcode from post content
            $truncate = preg_replace('@\[embed[^\]]*?\].*?\[\/embed]@si', '', $truncate);

            if ($strip_shortcodes) {
                $truncate = et_strip_shortcodes($truncate);
            } else {
                // apply content filters
                $truncate = apply_filters('the_content', $truncate);
            }

            // decide if we need to append dots at the end of the string
            if (strlen($truncate) <= $amount) {
                $echo_out = '';
            } else {
                $echo_out = '...';
                // $amount = $amount - 3;
            }

            // trim text to a certain number of characters, also remove spaces from the end of a string ( space counts as a character )
            $truncate = rtrim(et_wp_trim_words($truncate, $amount, ''));

            // remove the last word to make sure we display all words correctly
            if ('' != $echo_out) {
                $new_words_array = (array) explode(' ', $truncate);
                array_pop($new_words_array);

                $truncate = implode(' ', $new_words_array);

                // append dots to the end of the string
                $truncate .= $echo_out;
            }

            if ($echo) {
                echo $truncate;
            } else {
                return $truncate;
            }
        };
    }
}

add_action('admin_bar_menu', 'ggl_extra_adminbar', 999);

function ggl_extra_adminbar($wp_admin_bar)
{
    $current_user = wp_get_current_user();
    $args = array(
        'id'    => 'author_page',
        'title' => 'Perfil público',
        'href'  => get_author_posts_url($current_user->ID, $current_user->user_nicename),
        'meta'  => array( 'class' => 'item-menu-morado' ),
    );
    $wp_admin_bar->add_node($args);
}

//Función mostrar costo plugin eventos
//add_filter('tribe_events_admin_show_cost_field', '__return_true', 100);
