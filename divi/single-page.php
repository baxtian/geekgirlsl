<?php

global $layout_id;

$is_page_builder_used = false;
if (!empty($layout_id) || et_pb_is_pagebuilder_used(get_the_ID())) {
    $is_page_builder_used = true;
}

if (have_posts()) : the_post();

?>

<div id="main-content">

<?php if (! $is_page_builder_used && ! $layout_id) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">

<?php endif; ?>


				<?php if (! $is_page_builder_used && ! $layout_id) : ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<h1 class="entry-title main_title"><?php the_title(); ?></h1>
				<?php
                    $thumb = '';

                    $width = (int) apply_filters('et_pb_index_blog_image_width', 1080);

                    $height = (int) apply_filters('et_pb_index_blog_image_height', 675);
                    $classtext = 'et_featured_image';
                    $titletext = get_the_title();
                    $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                    $thumb = $thumbnail["thumb"];

                    if ('on' === et_get_option('divi_page_thumbnails', 'false') && '' !== $thumb) {
                        print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height);
                    }
                ?>

				<?php endif; ?>

					<div class="entry-content">
					<?php
                        if (! $is_page_builder_used && $layout_id) {
                            echo do_shortcode('[et_pb_section global_module="'.$layout_id.'"][/et_pb_section]');
                        } else {
                            the_content();
                        }

                        if (! $is_page_builder_used && ! $layout_id) {
                            wp_link_pages(array( 'before' => '<div class="page-links">' . esc_html__('Pages:', 'Divi'), 'after' => '</div>' ));
                        }
                    ?>
					</div> <!-- .entry-content -->

				<?php
                    if (! $is_page_builder_used && ! $layout_id && comments_open() && 'on' === et_get_option('divi_show_pagescomments', 'false')) {
                        comments_template('', true);
                    }
                ?>

				</article> <!-- .et_pb_post -->

<?php if (! $is_page_builder_used && ! $layout_id) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php endif; ?>
