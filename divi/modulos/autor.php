<?php

function gg_divi_modules_autor()
{
    class ET_Builder_Module_Autor extends ET_Builder_Module
    {
        public function init()
        {
            $this->name       = 'Autor';
            $this->slug       = 'et_pb_team_member';
            $this->fb_support = true;

            $this->whitelisted_fields = array(
                'name',
                'position',
                'image_url',
                'animation',
                'background_layout',
                'facebook_url',
                'twitter_url',
                'google_url',
                'linkedin_url',
                'content_new',
                'admin_label',
                'module_id',
                'module_class',
                'icon_color',
                'icon_hover_color',
            );

            $this->fields_defaults = array(
                'animation'         => array( 'off' ),
                'background_layout' => array( 'light' ),
            );

            $this->main_css_element = '%%order_class%%.et_pb_team_member';
            $this->advanced_options = array(
                'fonts' => array(
                    'header' => array(
                        'label'    => esc_html__('Header', 'et_builder'),
                        'css'      => array(
                            'main'      => "{$this->main_css_element} h4",
                            'important' => 'plugin_only',
                        ),
                    ),
                    'body'   => array(
                        'label'    => esc_html__('Body', 'et_builder'),
                        'css'      => array(
                            'main' => "{$this->main_css_element} *",
                        ),
                    ),
                ),
                'background' => array(
                    'settings' => array(
                        'color' => 'alpha',
                    ),
                ),
                'border' => array(),
                'custom_margin_padding' => array(
                    'css' => array(
                        'important' => 'all',
                    ),
                ),
            );
            $this->custom_css_options = array(
                'member_image' => array(
                    'label'    => esc_html__('Member Image', 'et_builder'),
                    'selector' => '.et_pb_team_member_image',
                ),
                'member_description' => array(
                    'label'    => esc_html__('Member Description', 'et_builder'),
                    'selector' => '.et_pb_team_member_description',
                ),
                'title' => array(
                    'label'    => esc_html__('Title', 'et_builder'),
                    'selector' => '.et_pb_team_member_description h4',
                ),
                'member_position' => array(
                    'label'    => esc_html__('Member Position', 'et_builder'),
                    'selector' => '.et_pb_member_position',
                ),
                'member_social_links' => array(
                    'label'    => esc_html__('Member Social Links', 'et_builder'),
                    'selector' => '.et_pb_member_social_links',
                ),
            );
        }

        public function get_fields()
        {
            $fields = array(
                'name' => array(
                    'label'           => esc_html__('Name', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input the name of the person', 'et_builder'),
                ),
                'position' => array(
                    'label'           => esc_html__('Position', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__("Input the person's position.", 'et_builder'),
                ),
                'image_url' => array(
                    'label'              => esc_html__('Image URL', 'et_builder'),
                    'type'               => 'upload',
                    'option_category'    => 'basic_option',
                    'upload_button_text' => esc_attr__('Upload an image', 'et_builder'),
                    'choose_text'        => esc_attr__('Choose an Image', 'et_builder'),
                    'update_text'        => esc_attr__('Set As Image', 'et_builder'),
                    'description'        => esc_html__('Upload your desired image, or type in the URL to the image you would like to display.', 'et_builder'),
                ),
                'animation' => array(
                    'label'             => esc_html__('Animation', 'et_builder'),
                    'type'              => 'select',
                    'option_category'   => 'configuration',
                    'options'           => array(
                        'off'     => esc_html__('No Animation', 'et_builder'),
                        'fade_in' => esc_html__('Fade In', 'et_builder'),
                        'left'    => esc_html__('Left To Right', 'et_builder'),
                        'right'   => esc_html__('Right To Left', 'et_builder'),
                        'top'     => esc_html__('Top To Bottom', 'et_builder'),
                        'bottom'  => esc_html__('Bottom To Top', 'et_builder'),
                    ),
                    'description'       => esc_html__('This controls the direction of the lazy-loading animation.', 'et_builder'),
                ),
                'background_layout' => array(
                    'label'           => esc_html__('Text Color', 'et_builder'),
                    'type'            => 'select',
                    'option_category' => 'color_option',
                    'options'           => array(
                        'light' => esc_html__('Dark', 'et_builder'),
                        'dark'  => esc_html__('Light', 'et_builder'),
                    ),
                    'description' => esc_html__('Here you can choose the value of your text. If you are working with a dark background, then your text should be set to light. If you are working with a light background, then your text should be dark.', 'et_builder'),
                ),
                'facebook_url' => array(
                    'label'           => esc_html__('Facebook Profile Url', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input Facebook Profile Url.', 'et_builder'),
                ),
                'twitter_url' => array(
                    'label'           => esc_html__('Twitter Profile Url', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input Twitter Profile Url', 'et_builder'),
                ),
                'google_url' => array(
                    'label'           => esc_html__('Google+ Profile Url', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input Google+ Profile Url', 'et_builder'),
                ),
                'linkedin_url' => array(
                    'label'           => esc_html__('LinkedIn Profile Url', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input LinkedIn Profile Url', 'et_builder'),
                ),
                'content_new' => array(
                    'label'           => esc_html__('Description', 'et_builder'),
                    'type'            => 'tiny_mce',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input the main text content for your module here.', 'et_builder'),
                ),
                'icon_color' => array(
                    'label'             => esc_html__('Icon Color', 'et_builder'),
                    'type'              => 'color',
                    'custom_color'      => true,
                    'tab_slug'          => 'advanced',
                ),
                'icon_hover_color' => array(
                    'label'             => esc_html__('Icon Hover Color', 'et_builder'),
                    'type'              => 'color',
                    'custom_color'      => true,
                    'tab_slug'          => 'advanced',
                ),
                'disabled_on' => array(
                    'label'           => esc_html__('Disable on', 'et_builder'),
                    'type'            => 'multiple_checkboxes',
                    'options'         => array(
                        'phone'   => esc_html__('Phone', 'et_builder'),
                        'tablet'  => esc_html__('Tablet', 'et_builder'),
                        'desktop' => esc_html__('Desktop', 'et_builder'),
                    ),
                    'additional_att'  => 'disable_on',
                    'option_category' => 'configuration',
                    'description'     => esc_html__('This will disable the module on selected devices', 'et_builder'),
                ),
                'admin_label' => array(
                    'label'       => esc_html__('Admin Label', 'et_builder'),
                    'type'        => 'text',
                    'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder'),
                ),
                'module_id' => array(
                    'label'           => esc_html__('CSS ID', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'configuration',
                    'tab_slug'        => 'custom_css',
                    'option_class'    => 'et_pb_custom_css_regular',
                ),
                'module_class' => array(
                    'label'           => esc_html__('CSS Class', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'configuration',
                    'tab_slug'        => 'custom_css',
                    'option_class'    => 'et_pb_custom_css_regular',
                ),
            );
            return $fields;
        }

        public function shortcode_callback($atts, $content = null, $function_name)
        {
            $module_id         = $this->shortcode_atts['module_id'];
            $module_class      = $this->shortcode_atts['module_class'];
            $name              = nl2br(get_the_author());
            $author_link       = get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename'));
            $position          = nl2br(get_the_author_meta('profesion'));
            $image_tag         = get_avatar(get_the_author_meta('ID'), 512);
            $animation         = $this->shortcode_atts['animation'];
            $facebook_url      = nl2br(get_the_author_meta('facebook'));
            $twitter_url       = nl2br(get_the_author_meta('twitter'));
            $google_url        = nl2br(get_the_author_meta('google_plus'));
            $linkedin_url      = nl2br(get_the_author_meta('linkedin'));
            $background_layout = $this->shortcode_atts['background_layout'];
            $icon_color        = $this->shortcode_atts['icon_color'];
            $icon_hover_color  = $this->shortcode_atts['icon_hover_color'];

            $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

            $image = $social_links = '';

            if ('' !== $icon_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector'    => '%%order_class%% .et_pb_member_social_links a',
                    'declaration' => sprintf(
                        'color: %1$s !important;',
                        esc_html($icon_color)
                    ),
                ));
            }

            if ('' !== $icon_hover_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector'    => '%%order_class%% .et_pb_member_social_links a:hover',
                    'declaration' => sprintf(
                        'color: %1$s !important;',
                        esc_html($icon_hover_color)
                    ),
                ));
            }

            if ('' !== $facebook_url) {
                $social_links .= sprintf(
                    '<li><a href="%1$s" class="et_pb_font_icon et_pb_facebook_icon"><span>%2$s</span></a></li>',
                    esc_url($facebook_url),
                    esc_html__('Facebook', 'et_builder')
                );
            }

            if ('' !== $twitter_url) {
                $social_links .= sprintf(
                    '<li><a href="%1$s" class="et_pb_font_icon et_pb_twitter_icon"><span>%2$s</span></a></li>',
                    esc_url($twitter_url),
                    esc_html__('Twitter', 'et_builder')
                );
            }

            if ('' !== $google_url) {
                $social_links .= sprintf(
                    '<li><a href="%1$s" class="et_pb_font_icon et_pb_google_icon"><span>%2$s</span></a></li>',
                    esc_url($google_url),
                    esc_html__('Google+', 'et_builder')
                );
            }

            if ('' !== $linkedin_url) {
                $social_links .= sprintf(
                    '<li><a href="%1$s" class="et_pb_font_icon et_pb_linkedin_icon"><span>%2$s</span></a></li>',
                    esc_url($linkedin_url),
                    esc_html__('LinkedIn', 'et_builder')
                );
            }

            if ('' !== $social_links) {
                $social_links = sprintf('<ul class="et_pb_member_social_links">%1$s</ul>', $social_links);
            }

            if ('' !== $image_tag) {
                $image = sprintf(
                    '<div class="et_pb_team_member_image et-waypoint%2$s">
                        %1$s
                    </div>',
                    $image_tag,
                    esc_attr(" et_pb_animation_{$animation}")
                );
            }

            $output = sprintf(
                '<div%3$s class="et_pb_module et_pb_team_member%4$s%9$s et_pb_bg_layout_%8$s clearfix">
                    %2$s
                    <div class="et_pb_team_member_description">
                        %5$s
                        %6$s
                        %1$s
                        %7$s
                    </div> <!-- .et_pb_team_member_description -->
                </div> <!-- .et_pb_team_member -->',
                nl2br(get_the_author_meta('description')),
                ('' !== $image ? $image : ''),
                ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''),
                ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''),
                ('' !== $name ? sprintf('<h4><a href="%2$s">%1$s</a></h4>', esc_html($name), $author_link) : ''),
                ('' !== $position ? sprintf('<p class="et_pb_member_position">%1$s</p>', esc_html($position)) : ''),
                $social_links,
                $background_layout,
                ('' === $image ? ' et_pb_team_member_no_image' : '')
            );

            return $output;
        }
    }
    new ET_Builder_Module_Autor;
}

add_action('et_builder_ready', 'gg_divi_modules_autor');
