<?php

function gg_divi_modules_historias()
{
    class ET_Builder_Module_Historias extends ET_Builder_Module
    {
        public function init()
        {
            $this->name       = 'Historias';
            $this->slug       = 'et_pb_historias';
            $this->fb_support = true;

            $this->whitelisted_fields = array(
                'encabezado',
                'button_text',
                'admin_label',
                'module_id',
                'module_class',
                'icon_color',
                'icon_hover_color',
            );

            $this->fields_defaults = array(
                'animation'         => array( 'off' ),
                'background_layout' => array( 'light' ),
            );

            $this->main_css_element = '%%order_class%%.et_pb_team_member';
            $this->advanced_options = array(
                'fonts' => array(
                    'header' => array(
                        'label'    => esc_html__('Header', 'et_builder'),
                        'css'      => array(
                            'main'      => "{$this->main_css_element} h4",
                            'important' => 'plugin_only',
                        ),
                    ),
                    'body'   => array(
                        'label'    => esc_html__('Body', 'et_builder'),
                        'css'      => array(
                            'main' => "{$this->main_css_element} *",
                        ),
                    ),
                ),
                'background' => array(
                    'settings' => array(
                        'color' => 'alpha',
                    ),
                ),
                'border' => array(),
                'custom_margin_padding' => array(
                    'css' => array(
                        'important' => 'all',
                    ),
                ),
            );
            $this->custom_css_options = array( );
        }

        public function get_fields()
        {
            $fields = array(
                'encabezado' => array(
                    'label'           => esc_html__('Encabezado', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input the title of the section', 'et_builder'),
                ),
                'button_text' => array(
                    'label'           => esc_html__('Button Text', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'basic_option',
                    'description'     => esc_html__('Input your desired button text.', 'et_builder'),
                ),
                'icon_color' => array(
                    'label'             => esc_html__('Icon Color', 'et_builder'),
                    'type'              => 'color',
                    'custom_color'      => true,
                    'tab_slug'          => 'advanced',
                ),
                'icon_hover_color' => array(
                    'label'             => esc_html__('Icon Hover Color', 'et_builder'),
                    'type'              => 'color',
                    'custom_color'      => true,
                    'tab_slug'          => 'advanced',
                ),
                'disabled_on' => array(
                    'label'           => esc_html__('Disable on', 'et_builder'),
                    'type'            => 'multiple_checkboxes',
                    'options'         => array(
                        'phone'   => esc_html__('Phone', 'et_builder'),
                        'tablet'  => esc_html__('Tablet', 'et_builder'),
                        'desktop' => esc_html__('Desktop', 'et_builder'),
                    ),
                    'additional_att'  => 'disable_on',
                    'option_category' => 'configuration',
                    'description'     => esc_html__('This will disable the module on selected devices', 'et_builder'),
                ),
                'admin_label' => array(
                    'label'       => esc_html__('Admin Label', 'et_builder'),
                    'type'        => 'text',
                    'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder'),
                ),
                'module_id' => array(
                    'label'           => esc_html__('CSS ID', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'configuration',
                    'tab_slug'        => 'custom_css',
                    'option_class'    => 'et_pb_custom_css_regular',
                ),
                'module_class' => array(
                    'label'           => esc_html__('CSS Class', 'et_builder'),
                    'type'            => 'text',
                    'option_category' => 'configuration',
                    'tab_slug'        => 'custom_css',
                    'option_class'    => 'et_pb_custom_css_regular',
                ),
            );
            return $fields;
        }

        public function shortcode_callback($atts, $content = null, $function_name)
        {
            $module_id         = $this->shortcode_atts['module_id'];
            $module_class      = $this->shortcode_atts['module_class'];
            $encabezado        = $this->shortcode_atts['encabezado'];
            $button_text        = $this->shortcode_atts['button_text'];
            //$animation         = $this->shortcode_atts['animation'];
            //$background_layout = $this->shortcode_atts['background_layout'];
            $icon_color        = $this->shortcode_atts['icon_color'];
            $icon_hover_color  = $this->shortcode_atts['icon_hover_color'];

            $args = array(
                'post_type' => 'historia',
                'posts_per_page' => 3
            );
            $historias = new WP_Query($args);

            $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

            $image = $social_links = '';

            if ('' !== $icon_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector'    => '%%order_class%% .et_pb_member_social_links a',
                    'declaration' => sprintf(
                        'color: %1$s !important;',
                        esc_html($icon_color)
                    ),
                ));
            }

            if ('' !== $icon_hover_color) {
                ET_Builder_Element::set_style($function_name, array(
                    'selector'    => '%%order_class%% .et_pb_member_social_links a:hover',
                    'declaration' => sprintf(
                        'color: %1$s !important;',
                        esc_html($icon_hover_color)
                    ),
                ));
            }

            $output = '<div class="seccion-testimonio et_pb_row_inner et_pb_row_inner_2 et_pb_row_1-4_1-4_1-4">
';
            if ($historias->have_posts()) :
            while ($historias->have_posts()) :
                $historias->the_post();
            $output .= '<div class="et_pb_column et_pb_column_1_4 et_pb_column_inner  et_pb_column_inner_3">

            <div class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center boton-borde-morado modulo-testimonio et_pb_cta_0" style="background-color: #f5f5f5;">
            <div class="et_pb_promo_description">


<p>'. get_the_post_thumbnail(null, 'thumbnail', array('class' => 'aligncenter', 'width' => '245', 'height' => '245')) .'</p>' . get_the_excerpt() . '

            </div>
            <a class="et_pb_promo_button et_pb_button" href="'. get_the_permalink() .'">'. esc_html($button_text) .'</a>
        </div>
        </div> <!-- .et_pb_column -->
';
            endwhile;
            endif;
            $output .= '</div> <!-- .et_pb_row_inner -->';

            $output = sprintf($output,
                ('' !== $encabezado ? $encabezado : ''),
                ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''),
                ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : '')
            );

            return $output;
        }
    }
    new ET_Builder_Module_Historias;
}

add_action('et_builder_ready', 'gg_divi_modules_historias');
