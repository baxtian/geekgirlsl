<?php get_header(); ?>

<?php

    global $layout_id;
    $layout_id = get_theme_mod('ggl_modulo_archive');

    //Si tiene un módulo asignado
    if (!$layout_id) {
        get_template_part('divi/archive');
    } elseif (have_posts()) {
        get_template_part('divi/single');
    } else {
        get_template_part('divi/single', 'sinresultados');
    }
?>

<?php get_footer();
