jQuery(document).ready(function() {

	//Geoposición
	/*jQuery('input.geo').geoLocationPicker({
	  left: "400px",
	  top: "-200px",
      gMapZoom: 11,
	  defaultLat: 5.068068,
	  defaultLng: -75.517391
	});*/

	jQuery('span[name=georeferenciar]').on('click', function() {
		var coordenadas = jQuery('input[name=geo_ref]').val();
		if(!coordenadas || coordenadas.length === 0) coordenadas = 'Manizales, Caldas, Colombia';
		lookupGeoData(coordenadas);
	});

	//Sugerencias
	jQuery('.sugerir').each( function() {
		sugerencia(this);
	})

	jQuery( document ).ajaxStop( function() {
		jQuery('.sugerir').each( function() {
			if(!jQuery(this).parent().hasClass('easy-autocomplete')) sugerencia(this);
		})
    } );

});

function lookupGeoData(coordenadas) {
	myGeoPositionGeoPicker({
		startAddress     : coordenadas,
		returnFieldMap   : {
							 'geo_ref' : '<LAT>,<LNG>',
							 'geo_ref_ref' : '<LAT>,<LNG>'
						   }
	});
}
