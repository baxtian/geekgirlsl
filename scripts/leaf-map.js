// See post: http://asmaloney.com/2014/01/code/creating-an-interactive-map-with-leaflet-and-openstreetmap/

var map = L.map('ggl_map', {
    minZoom: 2,
})

L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a', 'b', 'c']
}).addTo(map)

var myIcon = L.icon({
    iconUrl: leaflet.theme + '/imagenes/pin24.png',
    iconRetinaUrl: leaflet.theme + '/imagenes/pin48.png',
    iconSize: [24, 36],
    iconAnchor: [4, 33],
    popupAnchor: [0, -14]
})

var maxLat = -190;
var maxLng = -190;

var minLat = 190;
var minLng = 190;

jQuery.getJSON(et_pb_custom.ajaxurl + "?action=ggl_geojson", function(data) {
    jQuery.each(data, function(key, val) {
        L.marker( [val.lat, val.lng], {icon: myIcon} )
         .bindPopup( '<a href="' + val.url + '" target="_blank">' + val.name + '</a>' )
         .addTo( map );

         if(val.lat > maxLat) maxLat = parseFloat(val.lat);
         if(val.lat < minLat) minLat = parseFloat(val.lat);
         if(val.lng > maxLng) maxLng = parseFloat(val.lng);
         if(val.lng < minLng) minLng = parseFloat(val.lng);
    });

    map.fitBounds([[maxLat, maxLng],[minLat, minLng]], { padding: [50, 50] });
});
