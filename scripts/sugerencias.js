function sugerencia( element ) {
	var $input = jQuery(element);
	var type = $input.attr('data-type');
	if(type === undefined) type = 'post';
	var $id = $input.next();
	
	var mes = "";
	var attr = jQuery(element).attr('data-mes');
	if( typeof attr !== typeof undefined && attr !== false ) mes = "&mes=1";
	
	var options = {
	
		url: function(phrase) {
			return  ajaxurl + "?action=sugerencia&type=" + type + "&q=" + phrase + "" + mes;
		},
		listLocation: "items",
		matchResponseProperty: "query",	
		getValue: "name",
		requestDelay: 500,
				
		list: {
			maxNumberOfElements: 10000,
			match: {
				enabled: true
			},
			onChooseEvent: function() {
				var id = $input.getSelectedItemData().id;
				$id.val(id).trigger("change");
				$input.blur();
			},
		},
				
	};
	
	$input.easyAutocomplete(options);

	$input.on('keypress', function(e){
		if ( e.which == 13 ) return false; 
	});
}
