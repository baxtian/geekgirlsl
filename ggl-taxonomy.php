<?php

// No activar este plugin si no está instalado timber
include_once(ABSPATH . 'wp-admin/includes/plugin.php');
if (!is_plugin_active('timber-library/timber.php')) {
    add_action('admin_notices', function () {
        echo '<div id="message" class="error fade"><p style="line-height: 150%">';
        _e('<strong>GeekGirls</strong> requiere el plugin <strong>Timber</strong>', 'secciones');
        echo '</p></div>';
    });
    return;
}

include_once('inc/shortcodes.php');


add_action('add_meta_boxes', 'ggl_boxes');
add_action('save_post_nodo', 'ggl_save_datos_nodo');
add_action('save_post_project', 'ggl_save_datos_proyecto');
add_action('save_post_post', 'ggl_save_vinculacion_nodo');
add_action('save_post_project', 'ggl_save_vinculacion_nodo');
add_action('save_post_tribe_events', 'ggl_save_vinculacion_nodo');
add_action('save_post_historia', 'ggl_save_vinculacion_nodo');
add_action('save_post_historia', 'ggl_save_datos_historia');
add_action('admin_init', 'ggl_adminheader');
add_action('init', 'ggl_taxonomy');

add_action('category_add_form_fields', 'ggl_box_tax_layout');
add_action('category_edit_form_fields', 'ggl_box_tax_layout');
add_action('edit_category', 'ggl_save_tax_layout');
add_action('create_category', 'ggl_save_tax_layout');

add_action('admin_menu', 'ggl_ocultar_cajas');

add_action('wp_ajax_sugerencia', 'ggl_sugerencia');

add_filter('get_twig', 'ggl_addToTwig');
add_filter('et_builder_post_types', 'ggl_et_builder_post_types');

add_action('show_user_profile', 'ggl_vinculacion_profile_fields');
add_action('edit_user_profile', 'ggl_vinculacion_profile_fields');
add_action('personal_options_update', 'ggl_save_vinculacion_profile_fields');
add_action('edit_user_profile_update', 'ggl_save_vinculacion_profile_fields');

add_action('wp_ajax_ggl_geojson', 'ggl_geojson');
add_action('wp_ajax_nopriv_ggl_geojson', 'ggl_geojson');

add_filter('user_contactmethods', 'ggl_contactmethods', 10, 1);

add_action("after_switch_theme", 'ggl_agregar_rol');

add_action('admin_init', 'clean_unwanted_caps');
function clean_unwanted_caps()
{
    $delete_caps = array('edit_1', 'publish_1', 'edit_published_1', 'delete_1',
        'delete_published_1','edit_', 'publish_', 'edit_published_', 'delete_',
            'delete_published_', 'read_', 'edit_other_', 'read_private_');
    global $wp_roles;
    foreach ($delete_caps as $cap) {
        foreach (array_keys($wp_roles->roles) as $role) {
            $wp_roles->remove_cap($role, $cap);
        }
    }
}

//Fución para agregar el rol de lider
function ggl_agregar_rol()
{
    add_role('lider', 'Lider', array( 'read' => true, 'upload_files' => true, 'level_2' => true ));

    ggl_role('administrator', 'nodo', 'nodos', true, true, true);
    ggl_role('editor', 'nodo', 'nodos', true, true, true);

    ggl_role('lider', 'nodo', 'nodos');
    ggl_role('lider', 'post', 'posts', true, true);
    ggl_role('lider', 'tribe_events', 'tribe_events', true, true);

    ggl_disable_role('author', 'tribe_events', 'tribe_events');
}

function ggl_role($role, $post_type_name_singular, $post_type_name_plural, $can_publish = false, $can_delete = false, $editor = false)
{
    $_role = get_role($role);

    $_role->add_cap("edit_{$post_type_name_plural}");
    $_role->add_cap("edit_{$post_type_name_singular}");
    $_role->add_cap("read_{$post_type_name_singular}");
    $_role->add_cap("edit_published_{$post_type_name_plural}");

    if ($editor) {
        $_role->add_cap("edit_others_{$post_type_name_plural}");
        $_role->add_cap("edit_private_{$post_type_name_plural}");
        $_role->add_cap("read_private_{$post_type_name_plural}");
        $_role->add_cap("delete_others_{$post_type_name_plural}");
        $_role->add_cap("delete_private_{$post_type_name_plural}");
    }
    if ($can_publish) {
        $_role->add_cap("publish_{$post_type_name_plural}");
    }
    if ($can_delete) {
        $_role->add_cap("delete_{$post_type_name_plural}");
        $_role->add_cap("delete_{$post_type_name_singular}");
        $_role->add_cap("delete_published_{$post_type_name_plural}");
    }
}

function ggl_disable_role($role, $post_type_name_singular, $post_type_name_plural)
{
    $_role = get_role($role);

    $_role->remove_cap("edit_{$post_type_name_plural}");
    $_role->remove_cap("edit_{$post_type_name_singular}");
    $_role->remove_cap("read_{$post_type_name_singular}");
    $_role->remove_cap("edit_published_{$post_type_name_plural}");

    $_role->remove_cap("edit_others_{$post_type_name_plural}");
    $_role->remove_cap("edit_private_{$post_type_name_plural}");
    $_role->remove_cap("read_private_{$post_type_name_plural}");
    $_role->remove_cap("delete_others_{$post_type_name_plural}");
    $_role->remove_cap("delete_private_{$post_type_name_plural}");
    $_role->remove_cap("publish_{$post_type_name_plural}");
    $_role->remove_cap("delete_{$post_type_name_plural}");
    $_role->remove_cap("delete_{$post_type_name_singular}");
    $_role->remove_cap("delete_published_{$post_type_name_plural}");
}
// Función para agregar Twitter como método de contacto de un usuario
function ggl_contactmethods($contactmethods)
{
    // Agregar Twitter
    $contactmethods['profesion'] = 'Profesión';
    $contactmethods['facebook'] = 'Facebook';
    $contactmethods['twitter'] = 'Twitter';
    $contactmethods['linkedin'] = 'LinkdeIn';
    $contactmethods['google_plus'] = 'Google+';
    return $contactmethods;
}

// Función que declara los estilos y scripts para visualizar al
// administrar el sitio
function ggl_adminheader()
{
    wp_register_style('easy-autocomplete', get_bloginfo('stylesheet_directory')."/styles/easy-autocomplete.css", array(), GGL_VERSION);
    wp_register_style('geekgirls-admin', get_bloginfo('stylesheet_directory').'/styles/admin.css', array('easy-autocomplete'), GGL_VERSION);
    wp_enqueue_style('geekgirls-admin');

    /*wp_register_script('geo-api', "http://www.google.com/jsapi", array(), GGL_VERSION);
    wp_register_script('geo-sensor', "http://maps.google.com/maps/api/js?sensor=false", array('geo-api'), GGL_VERSION);
    wp_register_script('geo', get_bloginfo('stylesheet_directory')."/scripts/jquery.geolocationpicker.js", array('geo-sensor', 'jquery'), GGL_VERSION);*/
    wp_register_script('geo', "http://api.mygeoposition.com/api/geopicker/api.js", array('jquery'), GGL_VERSION);
    wp_register_script('easy-autocomplete', get_bloginfo('stylesheet_directory')."/scripts/jquery.easy-autocomplete.js", array('jquery'), GGL_VERSION);
    wp_register_script('sugerencias', get_bloginfo('stylesheet_directory')."/scripts/sugerencias.js", array('easy-autocomplete'), GGL_VERSION);
    wp_register_script('geekgirls-admin', get_bloginfo('stylesheet_directory')."/scripts/admin.js", array('geo', 'sugerencias'), GGL_VERSION);
    wp_enqueue_script('geekgirls-admin');
}

// Función para agregar la taxonomía
function ggl_taxonomy()
{
    //Registrar 'Nodo' como un nuevo tipo de información
    $labels = array(
        'name' => 'Nodo',
        'singular_name' => 'Nodo',
        'add_new' => 'Agregar nuevo',
        'add_new_item' => 'Agregar nuevo Nodo',
        'edit_item' => 'Editar Nodo',
        'new_item' => 'Nuevo Nodo',
        'view_item' => 'Ver Nodo',
        'search_items' => 'Buscar Nodo',
        'not_found' =>  'No se encontraron Nodos',
        'not_found_in_trash' => 'No se encontraron Nodos en la papelera',
        'parent_item_colon' => '',
        'menu_name' => 'Nodos',
    );

    $args = array(
        'query_var' => true,
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'map_meta_cap' => true,
        'capability_type' => array('nodo', 'nodos'),
        'taxonomies' => array(  ),
        'supports' => array('title', 'editor', 'thumbnail', 'author'),
        'menu_icon' => 'dashicons-location',
    );
    register_post_type('nodo', $args);

    //Registrar 'Historia' como un nuevo tipo de información
    $labels = array(
        'name' => 'Historia',
        'singular_name' => 'Historia / Testimonio',
        'add_new' => 'Agregar nueva',
        'add_new_item' => 'Agregar nueva Historia / Testimonio',
        'edit_item' => 'Editar Historia / Testimonio',
        'new_item' => 'Nueva Historia / Testimonio',
        'view_item' => 'Ver Historia / Testimonio',
        'search_items' => 'Buscar Historia / Testimonio',
        'not_found' =>  'No se encontraron Historias / Testimonios',
        'not_found_in_trash' => 'No se encontraron Historias / Testimonios en la papelera',
        'parent_item_colon' => '',
        'menu_name' => 'Historias y Testimonios',
    );

    $args = array(
        'query_var' => true,
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => 5,
        'taxonomies' => array(  ),
        'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
        'menu_icon' => 'dashicons-groups',
    );
    register_post_type('historia', $args);

    // Agregar nueva taxonomía para 'localizacion' y vincularla
    // con la entrada de tipo project
    $labels = array(
        'name' => 'Localización',
        'singular_name' => 'Localización',
        'search_items' =>  'Buscar Localizaciones',
        'all_items' => 'Todos las Localizaciones',
        'edit_item' => 'Editar Localización',
        'update_item' => 'Acualizar Localización',
        'add_new_item' => 'Agregar nueva Localización',
        'new_item_name' => 'Nombre de nueva Localización',
        'menu_name' => 'Localizaciones',
        'popular_items' => 'Localizaciones Populares',
        'add_or_remove_items' => 'Agregar o eliminar Localizaciones',
        'parent_item' => null, 'parent_item_colon' => null,
        'choose_from_most_used' => 'Elegir entre las Localizaciones más usados',
        'separate_items_with_commas' => 'Separe Localizaciones con comas'
    );

    register_taxonomy('localizacion', 'project', array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'rewrite' => array( 'slug' => 'localizacion' )
    ));

    // Agregar nueva taxonomía para 'país' y vincularla
    // con la entrada de tipo nodo
    $labels = array(
        'name' => 'País',
        'singular_name' => 'País',
        'search_items' =>  'Buscar Paises',
        'all_items' => 'Todos los Paises',
        'edit_item' => 'Editar País',
        'update_item' => 'Acualizar País',
        'add_new_item' => 'Agregar nuevo País',
        'new_item_name' => 'Nombre de nuevo País',
        'menu_name' => 'Paises',
        'popular_items' => 'Paises Populares',
        'add_or_remove_items' => 'Agregar o eliminar Paises',
        'parent_item' => null, 'parent_item_colon' => null,
        'choose_from_most_used' => 'Elegir entre los Paises más usados',
        'separate_items_with_commas' => 'Separe Paises con comas'
    );

    register_taxonomy('pais', 'nodo', array(
        'hierarchical' => false,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'rewrite' => array( 'slug' => 'pais' )
    ));
}

//Ocultar campo de localización en project
function ggl_ocultar_cajas()
{
    remove_meta_box('localizaciondiv', 'project', 'side');
    remove_meta_box('tagsdiv-pais', 'nodo', 'side');
}

// Función para crear las cajas deonde estarán las variables
function ggl_boxes()
{

    //Campos especiales para datos del nodo
    add_meta_box(
        'datos_nodo',
        'Nodo',
        'ggl_box_datos_nodo',
        'nodo'
    );

    //Campos especiales para datos del nodo
    add_meta_box(
        'datos_proyecto',
        'Proyecto',
        'ggl_box_datos_proyecto',
        'project'
    );

    //Campos especiales para datos de una hisotira o testimonio
    add_meta_box(
        'datos_historia',
        'Historia / Testimonio',
        'ggl_box_datos_historia',
        'historia'
    );

    //Campos especiales para vincular post a un nodo
    add_meta_box(
        'post_vinculacion_nodo',
        'Nodo',
        'ggl_box_vinculacion_nodo',
        'post'
    );

    //Campos especiales para vincular project a un nodo
    add_meta_box(
        'project_vinculacion_nodo',
        'Nodo',
        'ggl_box_vinculacion_nodo',
        'project'
    );

    //Campos especiales para vincular tribe_events a un nodo
    add_meta_box(
        'tribe_events_vinculacion_nodo',
        'Nodo',
        'ggl_box_vinculacion_nodo',
        'tribe_events'
    );

    //Campos especiales para vincular historia a un nodo
    add_meta_box(
        'historia_vinculacion_nodo',
        'Nodo',
        'ggl_box_vinculacion_nodo',
        'historia'
    );
}

// Caja especial para los datos del nodo.
function ggl_box_datos_nodo()
{
    global $post;

    $args = array();

    //Twig
    $args['geo_ref'] = get_post_meta($post->ID, 'geo_ref', true);
    $args['lider_nodo'] = get_post_meta($post->ID, 'lider_nodo', true);
    $paises = wp_get_post_terms($post->ID, 'pais');
    if ($paises) {
        $pais = $paises[0];
        $args['pais'] = $pais->name;
        $args['pais_id'] = $pais->term_id;
    }

    ggl_render("box-datos_nodo.twig", $args);
}

// Caja especial para los datos del nodo.
function ggl_box_datos_proyecto()
{
    global $post;

    $args = array();

    //Twig
    $localizaciones = wp_get_post_terms($post->ID, 'localizacion');
    if ($localizaciones) {
        $localizacion = $localizaciones[0];
        $args['localizacion'] = $localizacion->name;
        $args['localizacion_id'] = $localizacion->term_id;
    }

    ggl_render("box-datos_proyecto.twig", $args);
}

// Caja especial para los datos del nodo.
function ggl_box_vinculacion_nodo()
{
    global $post;

    $args = array();

    //Twig
    $_nodo = get_post_meta($post->ID, 'nodo', true);
    if ($_nodo) {
        $nodo = get_post($_nodo);
        $args['nodo'] = $nodo->post_title;
        $args['nodo_id'] = $nodo->id;
    }

    ggl_render("box-vinculacion_nodo.twig", $args);
}

// Caja especial para los datos del nodo.
function ggl_box_datos_historia()
{
    global $post;

    $args = array();

    //Twig
    $args['protagonista'] = get_post_meta($post->ID, 'protagonista', true);

    ggl_render("box-datos_historia.twig", $args);
}

// Tax box layout
function ggl_box_tax_layout($term)
{
    $args = array();

    //Lista de módulos disponibles
    $postargs = array('numberposts' => '-1', 'post_type' => 'et_pb_layout');
    $args['modulos'] = get_posts($postargs);

    //If there is an object is because we are editing
    if (is_object($term)) {
        //Data
        $args['layout'] = get_term_meta($term->term_id, 'layout', true);
        $args['edit'] = true;
    }

    // Twig
    ggl_render("taxterms-layout.twig", $args);
}

// Save tax box layout
function ggl_save_tax_layout($term_id)
{
    update_term_meta($term_id, 'layout', $_POST['layout']);
}

// Función para guardar los datos extra de los nodos
function ggl_save_datos_nodo($post_id)
{
    // Verificar que no sea una rutina de autoguardado.
    // Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
    // el proceso.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Revisar permisos del usuario
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Obtener el valor de la variable desde el formulario
    $geo_ref = $_POST['geo_ref'];
    $lider_nodo = $_POST['lider_nodo'];

    // Obtener el país
    $_pais = $_POST['pais'];
    $pais_id = ($_pais) ? $_pais : array();

    // Almacenar variables
    wp_set_post_terms($post_id, $localizacion_id, 'localizacion');

    // Almacenar variables
    update_post_meta($post_id, 'geo_ref', $geo_ref);
    update_post_meta($post_id, 'lider_nodo', $lider_nodo);
    wp_set_post_terms($post_id, $pais_id, 'pais');

    return;
}

// Función para guardar los datos extra de los proyecto
function ggl_save_datos_proyecto($post_id)
{
    // Verificar que no sea una rutina de autoguardado.
    // Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
    // el proceso.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Revisar permisos del usuario
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Obtener el valor de la variable desde el formulario
    $_localizacion = $_POST['localizacion'];
    $localizacion = get_term_by('name', $_localizacion, 'localizacion');
    $localizacion_id = ($localizacion) ? $localizacion->term_id : array();

    // Almacenar variables
    wp_set_post_terms($post_id, $localizacion_id, 'localizacion');

    return;
}

// Función para guardar los datos de vinculación a un nodo
function ggl_save_vinculacion_nodo($post_id)
{
    // Verificar que no sea una rutina de autoguardado.
    // Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
    // el proceso.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Revisar permisos del usuario
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Obtener el valor de la variable desde el formulario
    $nodo_id = $_POST['nodo_id'];

    // Almacenar variables
    update_post_meta($post_id, 'nodo', $nodo_id);

    return;
}

// Función para guardar los datos extra de las historias
function ggl_save_datos_historia($post_id)
{
    // Verificar que no sea una rutina de autoguardado.
    // Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
    // el proceso.
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

    // Revisar permisos del usuario
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }

    // Obtener el valor de la variable desde el formulario
    $protagonista = $_POST['protagonista'];

    // Almacenar variables
    update_post_meta($post_id, 'protagonista', $protagonista);

    return;
}

// Función para agregar campo 'nodo' a usuario

function ggl_vinculacion_profile_fields($user)
{

    //Twig
    $_nodo = get_the_author_meta('nodo', $user->ID);
    if ($_nodo) {
        $nodo = get_post($_nodo);
        $args['nodo'] = $nodo->post_title;
        $args['nodo_id'] = $nodo->id;
    }

    ggl_render("profile-vinculacion.twig", $args);
}

// Almacenar los datos de vinculacion
function ggl_save_vinculacion_profile_fields($user_id)
{
    if (! current_user_can('edit_user', $user_id)) {
        return false;
    }

    if (empty($_POST['nodo_id'])) {
        return false;
    }
    update_usermeta($user_id, 'nodo', $_POST['nodo_id']);
}

//Función para exportar el geojson con la ubicaciónd de los Nodos
function ggl_geojson()
{
    global $post;

    $answer = array();

    //Obtener los nodos
    $nodos = array();
    $query = new WP_Query(
        array( 'post_type' => 'nodo', 'posts_per_page' => -1 )
    );
    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            $geo_ref = get_post_meta($post->ID, 'geo_ref', true);
            if ($geo_ref) {
                $coord = explode(",", $geo_ref);
                $nodos[] = array(
                    'name' => $post->post_title,
                    'url' => get_permalink($post),
                    'lat' => $coord[0],
                    'lng' => $coord[1],
                );
            }
        }
    }

    echo json_encode($nodos);
    die();
}

function ggl_render($tpl, $args, $echo = true)
{
    //COnvertir en arreglo los argumentos para evitar errores
    if (!is_array($args)) {
        $args = array();
    }

    $context = Timber::get_context();
    //$context['plugin'] = array( 'directory' => plugin_dir_url(__FILE__) );
    Timber::$dirname = 'templates';

    //Render
    if ($echo) {
        return Timber::render($tpl, array_merge($context, $args));
    }
    // o feed?
    return Timber::fetch($tpl, array_merge($context, $args));
}

// Funciones especilaes para timber
function ggl_addToTwig($twig)
{
    /* this is where you can add your own fuctions to twig */
    $twig->addExtension(new Twig_Extension_StringLoader());
    $twig->addFilter(
        new Twig_SimpleFilter('usuarios', function ($id, $name, $atts = array()) {
            $atts = shortcode_atts(
                array(
                    'name' => $name,
                    'role__in' => '',
                    'selected' => $id,
                    'echo' => false
                ), $atts
            );
            $answer = wp_dropdown_users($atts);

            return $answer;
        })
    );
    return $twig;
}

function ggl_et_builder_post_types($post_types)
{
    $post_types[] = 'nodo';
    $post_types[] = 'historia';
    $post_types[] = 'tribe_events';

    return $post_types;
}

//Funció para crear lista de sugerencias
function ggl_sugerencia()
{
    $answer = array();

    $txt_type = array(
        'category' => 'Categoría',
        'post_tag' => 'Etiqueta',
        'pais' => 'País',
        'localizacion' => 'Localización',
        'post' => false,
        'page' => 'Página',
        'nodo' => 'Nodo'
    );

    if (isset($_GET['type'])) {
        $type = sanitize_key($_GET['type']);
        //$type = get_taxonomy( $type );
        if (! $type) {
            wp_die(0);
        }
        /*if ( ! current_user_can( $tax->cap->assign_terms ) )
            wp_die( -1 );*/
    } else {
        wp_die(0);
    }

    $s = wp_unslash($_GET['q']);

    $comma = _x(',', 'tag delimiter');
    if (',' !== $comma) {
        $s = str_replace($comma, ',', $s);
    }
    if (false !== strpos($s, ',')) {
        $s = explode(',', $s);
        $s = $s[count($s) - 1];
    }
    $s = trim($s);
    if (strlen($s) < 2) {
        wp_die();
    } // require 2 chars for matching*/

    $query = false;

    switch ($type) {
        case 'category':
        case 'post_tag':
        case 'pais':
        case 'localizacion':
            $query = get_terms($type, array( 'name__like' => $s, 'hide_empty' => false ));
            break;
        default:
            $args = array(
                'posts_per_page' => 20,
                'post_type' => array( $type ),
                's' => $s
            );
            break;
    }

    //¿Limitar a las de la última semana?
    if (isset($_GET['mes']) && $type != 'item_seccion') {
        $args['date_query'] = array(
            array(
                'column' => 'post_modified_gmt',
                'after' => '1 month ago',
            )
        );
    }

    if (!$query) {
        $query = new WP_Query($args);
    }

    if (isset($query->post_count) && $query->post_count == 0) {
        wp_die();
    }

    $results = array();
    switch ($type) {
        case 'category':
        case 'post_tag':
        case 'pais':
        case 'localizacion':
            foreach ($query as $item) {
                $results[] = array(
                    'name' => $item->name,
                    'id' => $item->term_id,
                    'type' => $txt_type[$type]
                );
            }
            break;
        default:
            foreach ($query->posts as $item) {
                $title = sprintf("%s (%s)", $item->post_title, $txt_type[$item->post_type]);
                if ($item->post_type == 'post') {
                    $title = $item->post_title;
                }
                $results[] = array(
                    'name' => $title,
                    'id' => $item->ID,
                    'type' => false,
                );
            }
            break;
    }

    $answer = array(
        'items' => $results,
        'query' => $s
    );
    echo json_encode($answer);
    wp_die();
}

/*************/
/* Add Divi Custom Post Settings box */
function ggl_add_meta_boxes()
{
    foreach (get_post_types() as $pt) {
        if (post_type_supports($pt, 'editor') and function_exists('et_single_settings_meta_box')) {
            add_meta_box('et_settings_meta_box', __('Divi Custom Post Settings', 'Divi'), 'et_single_settings_meta_box', $pt, 'side', 'high');
        }
    }
}
add_action('add_meta_boxes', 'ggl_add_meta_boxes');

/* Ensure Divi Builder appears in correct location */
function ggl_admin_js()
{
    $s = get_current_screen();
    if (!empty($s->post_type) and $s->post_type!='page' and $s->post_type!='post') {
        ?>
<script>
jQuery(function($){
	$('#et_pb_layout').insertAfter($('#et_pb_main_editor_wrap'));
});
</script>
<style>
#et_pb_layout { margin-top:20px; margin-bottom:0px }
</style>
<?php

    }
}
add_action('admin_head', 'ggl_admin_js');

// Ensure that Divi Builder framework is loaded - required for some post types when using Divi Builder plugin
add_filter('et_divi_role_editor_page', 'ggl_load_builder_on_all_page_types');
function ggl_load_builder_on_all_page_types($page)
{
    return isset($_GET['page'])?$_GET['page']:$page;
}
