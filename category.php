<?php get_header(); ?>

<?php

    $layout = false;
    $categories = get_the_category();
    if (isset($categories[0]->cat_ID)) {
        $category_id = $categories[0]->cat_ID;
        $layout = get_term_meta($category_id, 'layout', true);
    }

    //Si tiene un módulo asignado
    if (!$layout) {
        get_template_part('divi/archive');
    } else {
        global $layout_id;
        $layout_id = $layout;
        get_template_part('divi/single');
    }
?>

<?php get_footer();
