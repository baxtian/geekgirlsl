<?php

add_shortcode('ggl_map', 'ggl_map');
add_shortcode('ggl_titulo', 'ggl_titulo');
add_shortcode('ggl_contenido', 'ggl_contenido');
add_shortcode('ggl_fecha', 'ggl_fecha');
add_shortcode('ggl_etiquetas', 'ggl_etiquetas');

add_shortcode('ggl_team', 'ggl_team');
add_shortcode('ggl_team_slider', 'ggl_team_slider');

//Función para atender el shortcode del mapa
function ggl_map($atts)
{
    wp_enqueue_style('leafletjs');
    wp_enqueue_script('leafmap');
    wp_localize_script('leafletjs', 'leaflet', array(
        'theme' => get_bloginfo('stylesheet_directory'),
    ));
    return "<div id='ggl_map' style='width: 100%; height: 550px; border: 1px solid #AAA;'></div>";
}

function ggl_titulo($atts)
{
    global $post;
    return apply_filters('the_title', get_the_title($post));
}

function ggl_contenido($atts)
{
    $style = "";
    if (has_post_thumbnail()) {
        $url = get_the_post_thumbnail_url(null, 'full');
        $style = "<style>.fondo_img_destacada { background: url({$url}); }</style>";
    }

    return $style.apply_filters('the_content', get_the_content());
}

function ggl_fecha($atts)
{
    global $post;
    $args = shortcode_atts(array(
        'formato' => 'Y/m/d',
    ), $atts);

    return get_the_date($args['formato']);
}

function ggl_etiquetas($atts)
{
    global $post;
    return get_the_tag_list('', ', ', '');
}

function ggl_team($args)
{
    global $team_cat;
    $team_cat = isset($args['category']) ? $args['category'] : '';

    add_action('parse_tax_query', 'ggl_team_taxonomy');
    $answer = get_wp_tsas_showcase($args);
    remove_action('parse_tax_query', 'ggl_team_taxonomy');
    return $answer;
}

function ggl_team_slider($args)
{
    global $team_cat;
    $team_cat = isset($args['category']) ? $args['category'] : '';

    add_action('parse_tax_query', 'ggl_team_taxonomy');
    $answer = get_wp_tsas_showcase_slider($args);
    remove_action('parse_tax_query', 'ggl_team_taxonomy');
    return $answer;
}

function ggl_team_taxonomy($query)
{
    global $team_cat;
    //Si hay categoría, buscar los integrantes
    if ($team_cat != '') {
        $cat = array_map('intval', explode(',', $team_cat));
        if ($cat != "") {
            $tax_query = array( 'relation' => 'AND' );
            foreach ($cat as $item) {
                $tax_query[] = array( 'taxonomy' => 'tsas-category', 'field' => 'term_id', 'terms' => $item);
            }
        }
        $query->tax_query = new WP_Tax_Query($tax_query);
    }

    return $query;
}
