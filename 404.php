<?php get_header(); ?>

<div id="main-content">
				<article id="post-0" <?php post_class('et_pb_post not_found'); ?>>
					<?php if ($error = get_theme_mod('ggl_error_404')) {
    echo do_shortcode('[et_pb_section global_module="'.$error.'"][/et_pb_section]');
} else {
    get_template_part('includes/no-results', '404');
} ?>
				</article> <!-- .et_pb_post -->

</div> <!-- #main-content -->

<?php get_footer(); ?>
