<?php

function gg_divi_modules()
{
    if (class_exists('ET_Builder_Module')) {
        class ET_Builder_Module_Autor extends ET_Builder_Module
        {
            public function init()
            {
                $this->name       = 'Autor';
                $this->slug       = 'et_pb_team_member';
                $this->fb_support = true;

                $this->whitelisted_fields = array(
                    'name',
                    'position',
                    'image_url',
                    'animation',
                    'background_layout',
                    'facebook_url',
                    'twitter_url',
                    'google_url',
                    'linkedin_url',
                    'content_new',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'icon_color',
                    'icon_hover_color',
                );

                $this->fields_defaults = array(
                    'animation'         => array( 'off' ),
                    'background_layout' => array( 'light' ),
                );

                $this->main_css_element = '%%order_class%%.et_pb_team_member';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__('Header', 'et_builder'),
                            'css'      => array(
                                'main'      => "{$this->main_css_element} h4",
                                'important' => 'plugin_only',
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__('Body', 'et_builder'),
                            'css'      => array(
                                'main' => "{$this->main_css_element} *",
                            ),
                        ),
                    ),
                    'background' => array(
                        'settings' => array(
                            'color' => 'alpha',
                        ),
                    ),
                    'border' => array(),
                    'custom_margin_padding' => array(
                        'css' => array(
                            'important' => 'all',
                        ),
                    ),
                );
                $this->custom_css_options = array(
                    'member_image' => array(
                        'label'    => esc_html__('Member Image', 'et_builder'),
                        'selector' => '.et_pb_team_member_image',
                    ),
                    'member_description' => array(
                        'label'    => esc_html__('Member Description', 'et_builder'),
                        'selector' => '.et_pb_team_member_description',
                    ),
                    'title' => array(
                        'label'    => esc_html__('Title', 'et_builder'),
                        'selector' => '.et_pb_team_member_description h4',
                    ),
                    'member_position' => array(
                        'label'    => esc_html__('Member Position', 'et_builder'),
                        'selector' => '.et_pb_member_position',
                    ),
                    'member_social_links' => array(
                        'label'    => esc_html__('Member Social Links', 'et_builder'),
                        'selector' => '.et_pb_member_social_links',
                    ),
                );
            }

            public function get_fields()
            {
                $fields = array(
                    'name' => array(
                        'label'           => esc_html__('Name', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input the name of the person', 'et_builder'),
                    ),
                    'position' => array(
                        'label'           => esc_html__('Position', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__("Input the person's position.", 'et_builder'),
                    ),
                    'image_url' => array(
                        'label'              => esc_html__('Image URL', 'et_builder'),
                        'type'               => 'upload',
                        'option_category'    => 'basic_option',
                        'upload_button_text' => esc_attr__('Upload an image', 'et_builder'),
                        'choose_text'        => esc_attr__('Choose an Image', 'et_builder'),
                        'update_text'        => esc_attr__('Set As Image', 'et_builder'),
                        'description'        => esc_html__('Upload your desired image, or type in the URL to the image you would like to display.', 'et_builder'),
                    ),
                    'animation' => array(
                        'label'             => esc_html__('Animation', 'et_builder'),
                        'type'              => 'select',
                        'option_category'   => 'configuration',
                        'options'           => array(
                            'off'     => esc_html__('No Animation', 'et_builder'),
                            'fade_in' => esc_html__('Fade In', 'et_builder'),
                            'left'    => esc_html__('Left To Right', 'et_builder'),
                            'right'   => esc_html__('Right To Left', 'et_builder'),
                            'top'     => esc_html__('Top To Bottom', 'et_builder'),
                            'bottom'  => esc_html__('Bottom To Top', 'et_builder'),
                        ),
                        'description'       => esc_html__('This controls the direction of the lazy-loading animation.', 'et_builder'),
                    ),
                    'background_layout' => array(
                        'label'           => esc_html__('Text Color', 'et_builder'),
                        'type'            => 'select',
                        'option_category' => 'color_option',
                        'options'           => array(
                            'light' => esc_html__('Dark', 'et_builder'),
                            'dark'  => esc_html__('Light', 'et_builder'),
                        ),
                        'description' => esc_html__('Here you can choose the value of your text. If you are working with a dark background, then your text should be set to light. If you are working with a light background, then your text should be dark.', 'et_builder'),
                    ),
                    'facebook_url' => array(
                        'label'           => esc_html__('Facebook Profile Url', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input Facebook Profile Url.', 'et_builder'),
                    ),
                    'twitter_url' => array(
                        'label'           => esc_html__('Twitter Profile Url', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input Twitter Profile Url', 'et_builder'),
                    ),
                    'google_url' => array(
                        'label'           => esc_html__('Google+ Profile Url', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input Google+ Profile Url', 'et_builder'),
                    ),
                    'linkedin_url' => array(
                        'label'           => esc_html__('LinkedIn Profile Url', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input LinkedIn Profile Url', 'et_builder'),
                    ),
                    'content_new' => array(
                        'label'           => esc_html__('Description', 'et_builder'),
                        'type'            => 'tiny_mce',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input the main text content for your module here.', 'et_builder'),
                    ),
                    'icon_color' => array(
                        'label'             => esc_html__('Icon Color', 'et_builder'),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                    ),
                    'icon_hover_color' => array(
                        'label'             => esc_html__('Icon Hover Color', 'et_builder'),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__('Disable on', 'et_builder'),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__('Phone', 'et_builder'),
                            'tablet'  => esc_html__('Tablet', 'et_builder'),
                            'desktop' => esc_html__('Desktop', 'et_builder'),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__('This will disable the module on selected devices', 'et_builder'),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__('Admin Label', 'et_builder'),
                        'type'        => 'text',
                        'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder'),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__('CSS ID', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__('CSS Class', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            public function shortcode_callback($atts, $content = null, $function_name)
            {
                $module_id         = $this->shortcode_atts['module_id'];
                $module_class      = $this->shortcode_atts['module_class'];
                $name              = nl2br(get_the_author());
                $author_link       = get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename'));
                $position          = nl2br(get_the_author_meta('profesion'));
                $image_tag         = get_avatar(get_the_author_meta('ID'), 512);
                $animation         = $this->shortcode_atts['animation'];
                $facebook_url      = nl2br(get_the_author_meta('facebook'));
                $twitter_url       = nl2br(get_the_author_meta('twitter'));
                $google_url        = nl2br(get_the_author_meta('google_plus'));
                $linkedin_url      = nl2br(get_the_author_meta('linkedin'));
                $background_layout = $this->shortcode_atts['background_layout'];
                $icon_color        = $this->shortcode_atts['icon_color'];
                $icon_hover_color  = $this->shortcode_atts['icon_hover_color'];

                $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

                $image = $social_links = '';

                if ('' !== $icon_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector'    => '%%order_class%% .et_pb_member_social_links a',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html($icon_color)
                        ),
                    ));
                }

                if ('' !== $icon_hover_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector'    => '%%order_class%% .et_pb_member_social_links a:hover',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html($icon_hover_color)
                        ),
                    ));
                }

                if ('' !== $facebook_url) {
                    $social_links .= sprintf(
                        '<li><a href="%1$s" class="et_pb_font_icon et_pb_facebook_icon"><span>%2$s</span></a></li>',
                        esc_url($facebook_url),
                        esc_html__('Facebook', 'et_builder')
                    );
                }

                if ('' !== $twitter_url) {
                    $social_links .= sprintf(
                        '<li><a href="%1$s" class="et_pb_font_icon et_pb_twitter_icon"><span>%2$s</span></a></li>',
                        esc_url($twitter_url),
                        esc_html__('Twitter', 'et_builder')
                    );
                }

                if ('' !== $google_url) {
                    $social_links .= sprintf(
                        '<li><a href="%1$s" class="et_pb_font_icon et_pb_google_icon"><span>%2$s</span></a></li>',
                        esc_url($google_url),
                        esc_html__('Google+', 'et_builder')
                    );
                }

                if ('' !== $linkedin_url) {
                    $social_links .= sprintf(
                        '<li><a href="%1$s" class="et_pb_font_icon et_pb_linkedin_icon"><span>%2$s</span></a></li>',
                        esc_url($linkedin_url),
                        esc_html__('LinkedIn', 'et_builder')
                    );
                }

                if ('' !== $social_links) {
                    $social_links = sprintf('<ul class="et_pb_member_social_links">%1$s</ul>', $social_links);
                }

                if ('' !== $image_tag) {
                    $image = sprintf(
                        '<div class="et_pb_team_member_image et-waypoint%2$s">
        					%1$s
        				</div>',
                        $image_tag,
                        esc_attr(" et_pb_animation_{$animation}")
                    );
                }

                $output = sprintf(
                    '<div%3$s class="et_pb_module et_pb_team_member%4$s%9$s et_pb_bg_layout_%8$s clearfix">
        				%2$s
        				<div class="et_pb_team_member_description">
        					%5$s
        					%6$s
        					%1$s
        					%7$s
        				</div> <!-- .et_pb_team_member_description -->
        			</div> <!-- .et_pb_team_member -->',
                    nl2br(get_the_author_meta('description')),
                    ('' !== $image ? $image : ''),
                    ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''),
                    ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''),
                    ('' !== $name ? sprintf('<h4><a href="%2$s">%1$s</a></h4>', esc_html($name), $author_link) : ''),
                    ('' !== $position ? sprintf('<p class="et_pb_member_position">%1$s</p>', esc_html($position)) : ''),
                    $social_links,
                    $background_layout,
                    ('' === $image ? ' et_pb_team_member_no_image' : '')
                );

                return $output;
            }
        }
        new ET_Builder_Module_Autor;

        class ET_Builder_Module_Historias extends ET_Builder_Module
        {
            public function init()
            {
                $this->name       = 'Historias';
                $this->slug       = 'et_pb_historias';
                $this->fb_support = true;

                $this->whitelisted_fields = array(
                    'encabezado',
                    'button_text',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'icon_color',
                    'icon_hover_color',
                );

                $this->fields_defaults = array(
                    'animation'         => array( 'off' ),
                    'background_layout' => array( 'light' ),
                );

                $this->main_css_element = '%%order_class%%.et_pb_team_member';
                $this->advanced_options = array(
                    'fonts' => array(
                        'header' => array(
                            'label'    => esc_html__('Header', 'et_builder'),
                            'css'      => array(
                                'main'      => "{$this->main_css_element} h4",
                                'important' => 'plugin_only',
                            ),
                        ),
                        'body'   => array(
                            'label'    => esc_html__('Body', 'et_builder'),
                            'css'      => array(
                                'main' => "{$this->main_css_element} *",
                            ),
                        ),
                    ),
                    'background' => array(
                        'settings' => array(
                            'color' => 'alpha',
                        ),
                    ),
                    'border' => array(),
                    'custom_margin_padding' => array(
                        'css' => array(
                            'important' => 'all',
                        ),
                    ),
                );
                $this->custom_css_options = array( );
            }

            public function get_fields()
            {
                $fields = array(
                    'encabezado' => array(
                        'label'           => esc_html__('Encabezado', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input the title of the section', 'et_builder'),
                    ),
                    'button_text' => array(
                        'label'           => esc_html__('Button Text', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'basic_option',
                        'description'     => esc_html__('Input your desired button text.', 'et_builder'),
                    ),
                    'icon_color' => array(
                        'label'             => esc_html__('Icon Color', 'et_builder'),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                    ),
                    'icon_hover_color' => array(
                        'label'             => esc_html__('Icon Hover Color', 'et_builder'),
                        'type'              => 'color',
                        'custom_color'      => true,
                        'tab_slug'          => 'advanced',
                    ),
                    'disabled_on' => array(
                        'label'           => esc_html__('Disable on', 'et_builder'),
                        'type'            => 'multiple_checkboxes',
                        'options'         => array(
                            'phone'   => esc_html__('Phone', 'et_builder'),
                            'tablet'  => esc_html__('Tablet', 'et_builder'),
                            'desktop' => esc_html__('Desktop', 'et_builder'),
                        ),
                        'additional_att'  => 'disable_on',
                        'option_category' => 'configuration',
                        'description'     => esc_html__('This will disable the module on selected devices', 'et_builder'),
                    ),
                    'admin_label' => array(
                        'label'       => esc_html__('Admin Label', 'et_builder'),
                        'type'        => 'text',
                        'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder'),
                    ),
                    'module_id' => array(
                        'label'           => esc_html__('CSS ID', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                    'module_class' => array(
                        'label'           => esc_html__('CSS Class', 'et_builder'),
                        'type'            => 'text',
                        'option_category' => 'configuration',
                        'tab_slug'        => 'custom_css',
                        'option_class'    => 'et_pb_custom_css_regular',
                    ),
                );
                return $fields;
            }

            public function shortcode_callback($atts, $content = null, $function_name)
            {
                $module_id         = $this->shortcode_atts['module_id'];
                $module_class      = $this->shortcode_atts['module_class'];
                $encabezado        = $this->shortcode_atts['encabezado'];
                $button_text        = $this->shortcode_atts['button_text'];
                //$animation         = $this->shortcode_atts['animation'];
                //$background_layout = $this->shortcode_atts['background_layout'];
                $icon_color        = $this->shortcode_atts['icon_color'];
                $icon_hover_color  = $this->shortcode_atts['icon_hover_color'];

                $args = array(
                    'post_type' => 'historia',
                    'posts_per_page' => 3
                );
                $historias = new WP_Query($args);

                $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

                $image = $social_links = '';

                if ('' !== $icon_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector'    => '%%order_class%% .et_pb_member_social_links a',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html($icon_color)
                        ),
                    ));
                }

                if ('' !== $icon_hover_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector'    => '%%order_class%% .et_pb_member_social_links a:hover',
                        'declaration' => sprintf(
                            'color: %1$s !important;',
                            esc_html($icon_hover_color)
                        ),
                    ));
                }

                $output = '<div class="seccion-testimonio et_pb_row_inner et_pb_row_inner_2 et_pb_row_1-4_1-4_1-4">
';
                if ($historias->have_posts()) :
                while ($historias->have_posts()) :
                    $historias->the_post();
                $output .= '<div class="et_pb_column et_pb_column_1_4 et_pb_column_inner  et_pb_column_inner_3">

				<div class="et_pb_promo et_pb_module et_pb_bg_layout_light et_pb_text_align_center boton-borde-morado modulo-testimonio et_pb_cta_0" style="background-color: #f5f5f5;">
				<div class="et_pb_promo_description">


<p>'. get_the_post_thumbnail(null, 'thumbnail', array('class' => 'aligncenter', 'width' => '245', 'height' => '245')) .'</p>' . get_the_excerpt() . '

				</div>
				<a class="et_pb_promo_button et_pb_button" href="'. get_the_permalink() .'">'. esc_html($button_text) .'</a>
			</div>
			</div> <!-- .et_pb_column -->
';
                endwhile;
                endif;
                $output .= '</div> <!-- .et_pb_row_inner -->';

                $output = sprintf($output,
                    ('' !== $encabezado ? $encabezado : ''),
                    ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''),
                    ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : '')
                );

                return $output;
            }
        }
        new ET_Builder_Module_Historias;

        class ET_Builder_Module_Eventos extends ET_Builder_Module
        {
            public function init()
            {
                $this->name       = esc_html__('Eventos', 'et_builder');
                $this->slug       = 'et_pb_eventos';
                $this->fb_support = true;

                $this->whitelisted_fields = array(
                    'fullwidth',
                    'posts_number',
                    'include_categories',
                    'meta_date',
                    'show_thumbnail',
                    'show_content',
                    'show_more',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'show_pagination',
                    'offset_number',
                    'background_layout',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'masonry_tile_background_color',
                    'use_dropshadow',
                    'use_overlay',
                    'overlay_icon_color',
                    'hover_overlay_color',
                    'hover_icon'
                );

                $this->fields_defaults = array(
                    'fullwidth' => array(
                        'on'
                    ),
                    'posts_number' => array(
                        10,
                        'add_default_setting'
                    ),
                    'meta_date' => array(
                        'M j, Y',
                        'add_default_setting'
                    ),
                    'show_thumbnail' => array(
                        'on'
                    ),
                    'show_content' => array(
                        'off'
                    ),
                    'show_more' => array(
                        'off'
                    ),
                    'show_author' => array(
                        'on'
                    ),
                    'show_date' => array(
                        'on'
                    ),
                    'show_categories' => array(
                        'on'
                    ),
                    'show_comments' => array(
                        'off'
                    ),
                    'show_pagination' => array(
                        'on'
                    ),
                    'offset_number' => array(
                        0,
                        'only_default_setting'
                    ),
                    'background_layout' => array(
                        'light'
                    ),
                    'use_dropshadow' => array(
                        'off'
                    ),
                    'use_overlay' => array(
                        'off'
                    )
                );

                $this->main_css_element   = '%%order_class%% .et_pb_post';
                $this->advanced_options   = array(
                    'fonts' => array(
                        'header' => array(
                            'label' => esc_html__('Header', 'et_builder'),
                            'css' => array(
                                'main' => "{$this->main_css_element} .entry-title",
                                'plugin_main' => "{$this->main_css_element} .entry-title, {$this->main_css_element} .entry-title a",
                                'important' => 'all'
                            )
                        ),
                        'meta' => array(
                            'label' => esc_html__('Meta', 'et_builder'),
                            'css' => array(
                                'main' => "{$this->main_css_element} .post-meta"
                            )
                        ),
                        'body' => array(
                            'label' => esc_html__('Body', 'et_builder'),
                            'css' => array(
                                'color' => "{$this->main_css_element}, {$this->main_css_element} .post-content *",
                                'line_height' => "{$this->main_css_element} p"
                            )
                        )
                    ),
                    'border' => array(
                        'css' => array(
                            'main' => "%%order_class%%.et_pb_module .et_pb_post",
                            'important' => 'plugin_only'
                        )
                    )
                );
                $this->custom_css_options = array(
                    'title' => array(
                        'label' => esc_html__('Title', 'et_builder'),
                        'selector' => '.et_pb_post .entry-title'
                    ),
                    'post_meta' => array(
                        'label' => esc_html__('Post Meta', 'et_builder'),
                        'selector' => '.et_pb_post .post-meta'
                    ),
                    'pagenavi' => array(
                        'label' => esc_html__('Pagenavi', 'et_builder'),
                        'selector' => '.wp_pagenavi'
                    ),
                    'featured_image' => array(
                        'label' => esc_html__('Featured Image', 'et_builder'),
                        'selector' => '.et_pb_image_container'
                    ),
                    'read_more' => array(
                        'label' => esc_html__('Read More Button', 'et_builder'),
                        'selector' => '.et_pb_post .more-link'
                    )
                );
            }

            public function get_fields()
            {
                $fields = array(
                    'fullwidth' => array(
                        'label' => esc_html__('Layout', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'layout',
                        'options' => array(
                            'on' => esc_html__('Fullwidth', 'et_builder'),
                            'off' => esc_html__('Grid', 'et_builder')
                        ),
                        'affects' => array(
                            'background_layout',
                            'use_dropshadow',
                            'masonry_tile_background_color'
                        ),
                        'description' => esc_html__('Toggle between the various blog layout types.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'posts_number' => array(
                        'label' => esc_html__('Posts Number', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('Choose how much posts you would like to display per page.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'include_categories' => array(
                        'label' => esc_html__('Include Categories', 'et_builder'),
                        'renderer' => 'et_builder_include_categories_option',
                        'option_category' => 'basic_option',
                        'renderer_options' => array(
                            'use_terms' => false
                        ),
                        'description' => esc_html__('Choose which categories you would like to include in the feed.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'meta_date' => array(
                        'label' => esc_html__('Meta Date Format', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_thumbnail' => array(
                        'label' => esc_html__('Show Featured Image', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('This will turn thumbnails on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_content' => array(
                        'label' => esc_html__('Content', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'configuration',
                        'options' => array(
                            'off' => esc_html__('Show Excerpt', 'et_builder'),
                            'on' => esc_html__('Show Content', 'et_builder')
                        ),
                        'affects' => array(
                            'show_more'
                        ),
                        'description' => esc_html__('Showing the full content will not truncate your posts on the index page. Showing the excerpt will only display your excerpt text.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_more' => array(
                        'label' => esc_html__('Read More Button', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'depends_show_if' => 'off',
                        'description' => esc_html__('Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_author' => array(
                        'label' => esc_html__('Show Author', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn on or off the author link.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_date' => array(
                        'label' => esc_html__('Show Date', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn the date on or off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_categories' => array(
                        'label' => esc_html__('Show Categories', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn the category links on or off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_comments' => array(
                        'label' => esc_html__('Show Comment Count', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn comment count on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_pagination' => array(
                        'label' => esc_html__('Show Pagination', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn pagination on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'offset_number' => array(
                        'label' => esc_html__('Offset Number', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('Choose how many posts you would like to offset by', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'use_overlay' => array(
                        'label' => esc_html__('Featured Image Overlay', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'layout',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'affects' => array(
                            'overlay_icon_color',
                            'hover_overlay_color',
                            'hover_icon'
                        ),
                        'description' => esc_html__('If enabled, an overlay color and icon will be displayed when a visitors hovers over the featured image of a post.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'overlay_icon_color' => array(
                        'label' => esc_html__('Overlay Icon Color', 'et_builder'),
                        'type' => 'color',
                        'custom_color' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom color for the overlay icon', 'et_builder')
                    ),
                    'hover_overlay_color' => array(
                        'label' => esc_html__('Hover Overlay Color', 'et_builder'),
                        'type' => 'color-alpha',
                        'custom_color' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom color for the overlay', 'et_builder')
                    ),
                    'hover_icon' => array(
                        'label' => esc_html__('Hover Icon Picker', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'class' => array(
                            'et-pb-font-icon'
                        ),
                        'renderer' => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom icon for the overlay', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'background_layout' => array(
                        'label' => esc_html__('Text Color', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'color_option',
                        'options' => array(
                            'light' => esc_html__('Dark', 'et_builder'),
                            'dark' => esc_html__('Light', 'et_builder')
                        ),
                        'depends_default' => true,
                        'description' => esc_html__('Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder')
                    ),
                    'masonry_tile_background_color' => array(
                        'label' => esc_html__('Grid Tile Background Color', 'et_builder'),
                        'type' => 'color-alpha',
                        'custom_color' => true,
                        'tab_slug' => 'advanced',
                        'depends_show_if' => 'off',
                        'depends_to' => array(
                            'fullwidth'
                        )
                    ),
                    'use_dropshadow' => array(
                        'label' => esc_html__('Use Dropshadow', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'layout',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'tab_slug' => 'advanced',
                        'depends_show_if' => 'off',
                        'depends_to' => array(
                            'fullwidth'
                        )
                    ),
                    'disabled_on' => array(
                        'label' => esc_html__('Disable on', 'et_builder'),
                        'type' => 'multiple_checkboxes',
                        'options' => array(
                            'phone' => esc_html__('Phone', 'et_builder'),
                            'tablet' => esc_html__('Tablet', 'et_builder'),
                            'desktop' => esc_html__('Desktop', 'et_builder')
                        ),
                        'additional_att' => 'disable_on',
                        'option_category' => 'configuration',
                        'description' => esc_html__('This will disable the module on selected devices', 'et_builder')
                    ),
                    'admin_label' => array(
                        'label' => esc_html__('Admin Label', 'et_builder'),
                        'type' => 'text',
                        'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder')
                    ),
                    'module_id' => array(
                        'label' => esc_html__('CSS ID', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'tab_slug' => 'custom_css',
                        'option_class' => 'et_pb_custom_css_regular'
                    ),
                    'module_class' => array(
                        'label' => esc_html__('CSS Class', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'tab_slug' => 'custom_css',
                        'option_class' => 'et_pb_custom_css_regular'
                    ),
                    '__posts' => array(
                        'type' => 'computed',
                        'computed_callback' => array(
                            'ET_Builder_Module_Blog',
                            'get_blog_posts'
                        ),
                        'computed_depends_on' => array(
                            'fullwidth',
                            'posts_number',
                            'include_categories',
                            'meta_date',
                            'show_thumbnail',
                            'show_content',
                            'show_more',
                            'show_author',
                            'show_date',
                            'show_categories',
                            'show_comments',
                            'show_pagination',
                            'offset_number',
                            'use_overlay',
                            'hover_icon'
                        )
                    )
                );
                return $fields;
            }

            /**
             * Get blog posts for blog module
             *
             * @param array   arguments that is being used by et_pb_blog
             * @return string blog post markup
             */
            public static function get_blog_posts($args = array(), $conditional_tags = array(), $current_page = array())
            {
                global $paged, $post, $wp_query, $et_fb_processing_shortcode_object, $et_pb_rendering_column_content;

                $global_processing_original_value = $et_fb_processing_shortcode_object;

                // Default params are combination of attributes that is used by et_pb_blog and
                // conditional tags that need to be simulated (due to AJAX nature) by passing args
                $defaults = array(
                    'fullwidth' => '',
                    'posts_number' => '',
                    'include_categories' => '',
                    'meta_date' => '',
                    'show_thumbnail' => '',
                    'show_content' => '',
                    'show_author' => '',
                    'show_date' => '',
                    'show_categories' => '',
                    'show_comments' => '',
                    'show_pagination' => '',
                    'background_layout' => '',
                    'show_more' => '',
                    'offset_number' => '',
                    'masonry_tile_background_color' => '',
                    'use_dropshadow' => '',
                    'overlay_icon_color' => '',
                    'hover_overlay_color' => '',
                    'hover_icon' => '',
                    'use_overlay' => ''
                );

                // WordPress' native conditional tag is only available during page load. It'll fail during component update because
                // et_pb_process_computed_property() is loaded in admin-ajax.php. Thus, use WordPress' conditional tags on page load and
                // rely to passed $conditional_tags for AJAX call
                $is_front_page               = et_fb_conditional_tag('is_front_page', $conditional_tags);
                $is_search                   = et_fb_conditional_tag('is_search', $conditional_tags);
                $is_single                   = et_fb_conditional_tag('is_single', $conditional_tags);
                $et_is_builder_plugin_active = et_fb_conditional_tag('et_is_builder_plugin_active', $conditional_tags);

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters('wp_audio_shortcode_library');
                remove_all_filters('wp_audio_shortcode');
                remove_all_filters('wp_audio_shortcode_class');

                $args = wp_parse_args($args, $defaults);

                $overlay_output = '';
                $hover_icon     = '';

                if ('on' === $args['use_overlay']) {
                    $data_icon = '' !== $args['hover_icon'] ? sprintf(' data-icon="%1$s"', esc_attr(et_pb_process_font_icon($args['hover_icon']))) : '';

                    $overlay_output = sprintf('<span class="et_overlay%1$s"%2$s></span>', ('' !== $args['hover_icon'] ? ' et_pb_inline_icon' : ''), $data_icon);
                }

                $overlay_class = 'on' === $args['use_overlay'] ? ' et_pb_has_overlay' : '';

                $query_args = array(
                    'posts_per_page' => intval($args['posts_number']),
                    'post_status' => 'publish'
                );

                if (defined('DOING_AJAX') && isset($current_page['paged'])) {
                    $paged = intval($current_page['paged']);
                } else {
                    $paged = $is_front_page ? get_query_var('page') : get_query_var('paged');
                }

                if ('' !== $args['include_categories']) {
                    $query_args['cat'] = $args['include_categories'];
                }

                if (!$is_search) {
                    $query_args['paged'] = $paged;
                }

                if ('' !== $args['offset_number'] && !empty($args['offset_number'])) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ($paged > 1) {
                        $query_args['offset'] = (($paged - 1) * intval($args['posts_number'])) + intval($args['offset_number']);
                    } else {
                        $query_args['offset'] = intval($args['offset_number']);
                    }

                    /**
                     * If no category selected and it is ajax request, the offset starts from zero instead of one. Adjust accordingly.
                     */
                    if (!isset($query_args['cat']) && defined('DOING_AJAX')) {
                        $query_args['offset'] = intval($query_args['offset']) + 1;
                    }
                }

                if ($is_single) {
                    $query_args['post__not_in'][] = get_the_ID();
                }

                $args['meta_query'][] = array(
                    'key'     => '_EventStartDate',
                    'value'   => date('Y-m-d H:i:s'),
                    'compare' => '<',
                );

                $args['post_type'] = 'tribe_events';

                ob_start();

                $events = new WP_Query($args);

                if ($events->have_posts()) {
                    while ($events->have_posts()) {
                        $events->the_post();
                        global $et_fb_processing_shortcode_object;

                        $global_processing_original_value = $et_fb_processing_shortcode_object;

                        // reset the fb processing flag
                        $et_fb_processing_shortcode_object = false;

                        $thumb          = '';
                        $width          = 'on' === $args['fullwidth'] ? 1080 : 400;
                        $width          = (int) apply_filters('et_pb_blog_image_width', $width);
                        $height         = 'on' === $args['fullwidth'] ? 675 : 250;
                        $height         = (int) apply_filters('et_pb_blog_image_height', $height);
                        $classtext      = 'on' === $args['fullwidth'] ? 'et_pb_post_main_image' : '';
                        $titletext      = get_the_title();
                        $thumbnail      = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                        $thumb          = $thumbnail["thumb"];
                        $no_thumb_class = '' === $thumb || 'off' === $args['show_thumbnail'] ? ' et_pb_no_thumb' : '';

                        $post_format = et_pb_post_format();
                        if (in_array($post_format, array(
                            'video',
                            'gallery'
                        ))) {
                            $no_thumb_class = '';
                        }

                        // Print output
?>
        	<article id="" <?php
                        post_class('et_pb_post clearfix' . $no_thumb_class . $overlay_class); ?>>
        	<?php
                        et_divi_post_format_content();

                        if (!in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            if ('video' === $post_format && false !== ($first_video = et_get_first_video())):
                                $video_overlay = has_post_thumbnail() ? sprintf('<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
        	<div class="et_pb_video_overlay_hover">
        	<a href="#" class="et_pb_video_play"></a>
        	</div>
        	</div>', $thumb) : '';
                            printf('<div class="et_main_video_container">
        	%1$s
        	%2$s
        	</div>', $video_overlay, $first_video); elseif ('gallery' === $post_format):
                                et_pb_gallery_images('slider'); elseif ('' !== $thumb && 'on' === $args['show_thumbnail']):
                                if ('on' !== $args['fullwidth']) {
                                    echo '<div class="et_pb_image_container">';
                                } ?>
        	<a href="<?php
                                esc_url(the_permalink()); ?>" class="entry-featured-image-url">
        	<?php
                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
        	<?php
                                if ('on' === $args['use_overlay']) {
                                    echo $overlay_output;
                                } ?>
        	</a>
        	<?php
                                if ('on' !== $args['fullwidth']) {
                                    echo '</div> <!-- .et_pb_image_container -->';
                                }
                            endif;
                        } ?>

        	<?php
                        if ('off' === $args['fullwidth'] || !in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            ?>
        	<?php
                            if (!in_array($post_format, array(
                                'link',
                                'audio'
                            ))) {
                                ?>
        	<h2 class="entry-title"><a href="<?php
                                esc_url(the_permalink()); ?>"><?php
                                the_title(); ?></a></h2>
        	<?php

                            } ?>

        	<?php
                            if ('on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories'] || 'on' === $args['show_comments']) {
                                printf('<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>', ('on' === $args['show_author'] ? et_get_safe_localization(sprintf(__('by %s', 'et_builder'), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>')) : ''), (('on' === $args['show_author'] && 'on' === $args['show_date']) ? ' | ' : ''), ('on' === $args['show_date'] ? et_get_safe_localization(sprintf(__('%s', 'et_builder'), '<span class="published">' . esc_html(get_the_date($args['meta_date'])) . '</span>')) : ''), ((('on' === $args['show_author'] || 'on' === $args['show_date']) && 'on' === $args['show_categories']) ? ' | ' : ''), ('on' === $args['show_categories'] ? get_the_category_list(', ') : ''), ((('on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories']) && 'on' === $args['show_comments']) ? ' | ' : ''), ('on' === $args['show_comments'] ? sprintf(esc_html(_nx('1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder')), number_format_i18n(get_comments_number())) : ''));
                            }

                            $post_content = et_strip_shortcodes(et_delete_post_first_video(get_the_content()), true);

                            // reset the fb processing flag
                            $et_fb_processing_shortcode_object = false;
                            // set the flag to indicate that we're processing internal content
                            $et_pb_rendering_column_content    = true;
                            // reset all the attributes required to properly generate the internal styles
                            ET_Builder_Element::clean_internal_modules_styles();

                            echo '<div class="post-content">';

                            if ('on' === $args['show_content']) {
                                global $more;

                                // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                if (et_pb_is_pagebuilder_used(get_the_ID())) {
                                    $more = 1;

                                    echo apply_filters('the_content', $post_content);
                                } else {
                                    $more = null;
                                    echo apply_filters('the_content', et_delete_post_first_video(get_the_content(esc_html__('read more...', 'et_builder'))));
                                }
                            } else {
                                if (has_excerpt()) {
                                    the_excerpt();
                                } else {
                                    if ('' !== $post_content) {
                                        // set the $et_fb_processing_shortcode_object to false, to retrieve the content inside truncate_post() correctly
                                        $et_fb_processing_shortcode_object = false;
                                        echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));
                                        // reset the $et_fb_processing_shortcode_object to its original value
                                        $et_fb_processing_shortcode_object = $global_processing_original_value;
                                    } else {
                                        echo '';
                                    }
                                }
                            }

                            $et_fb_processing_shortcode_object = $global_processing_original_value;
                            // retrieve the styles for the modules inside Blog content
                            $internal_style                    = ET_Builder_Element::get_style(true);
                            // reset all the attributes after we retrieved styles
                            ET_Builder_Element::clean_internal_modules_styles(false);
                            $et_pb_rendering_column_content = false;
                            // append styles to the blog content
                            if ($internal_style) {
                                printf('<style type="text/css" class="et_fb_blog_inner_content_styles">
        	%1$s
        	</style>', $internal_style);
                            }

                            echo '</div>';

                            if ('on' !== $args['show_content']) {
                                $more = 'on' == $args['show_more'] ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
                                echo $more;
                            } ?>
        	<?php

                        } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery'
?>
        	</article>
        	<?php

                        $et_fb_processing_shortcode_object = $global_processing_original_value;
                    } // endwhile

                    if ('on' === $args['show_pagination'] && !$is_search) {
                        // echo '</div> <!-- .et_pb_posts -->'; // @todo this causes closing tag issue

                        $container_is_closed = true;

                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi(array(
                                'query' => $query
                            ));
                        } else {
                            if ($et_is_builder_plugin_active) {
                                include(ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php');
                            } else {
                                get_template_part('includes/navigation', 'index');
                            }
                        }
                    }

                    wp_reset_query();
                } else {
                    if ($et_is_builder_plugin_active) {
                        include(ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php');
                    } else {
                        get_template_part('includes/no-results', 'index');
                    }
                }

                wp_reset_postdata();

                // Reset $wp_query to its origin
                $wp_query = $wp_query_page;

                $posts = ob_get_contents();

                ob_end_clean();

                return $posts;
            }

            public function shortcode_callback($atts, $content = null, $function_name)
            {
                /**
                 * Cached $wp_filter so it can be restored at the end of the callback.
                 * This is needed because this callback uses the_content filter / calls a function
                 * which uses the_content filter. WordPress doesn't support nested filter
                 */
                global $wp_filter;
                $wp_filter_cache = $wp_filter;

                $module_id                     = $this->shortcode_atts['module_id'];
                $module_class                  = $this->shortcode_atts['module_class'];
                $fullwidth                     = $this->shortcode_atts['fullwidth'];
                $posts_number                  = $this->shortcode_atts['posts_number'];
                $include_categories            = $this->shortcode_atts['include_categories'];
                $meta_date                     = $this->shortcode_atts['meta_date'];
                $show_thumbnail                = $this->shortcode_atts['show_thumbnail'];
                $show_content                  = $this->shortcode_atts['show_content'];
                $show_author                   = $this->shortcode_atts['show_author'];
                $show_date                     = $this->shortcode_atts['show_date'];
                $show_categories               = $this->shortcode_atts['show_categories'];
                $show_comments                 = $this->shortcode_atts['show_comments'];
                $show_pagination               = $this->shortcode_atts['show_pagination'];
                $background_layout             = $this->shortcode_atts['background_layout'];
                $show_more                     = $this->shortcode_atts['show_more'];
                $offset_number                 = $this->shortcode_atts['offset_number'];
                $masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
                $use_dropshadow                = $this->shortcode_atts['use_dropshadow'];
                $overlay_icon_color            = $this->shortcode_atts['overlay_icon_color'];
                $hover_overlay_color           = $this->shortcode_atts['hover_overlay_color'];
                $hover_icon                    = $this->shortcode_atts['hover_icon'];
                $use_overlay                   = $this->shortcode_atts['use_overlay'];

                global $paged;

                $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters('wp_audio_shortcode_library');
                remove_all_filters('wp_audio_shortcode');
                remove_all_filters('wp_audio_shortcode_class');

                if ('' !== $masonry_tile_background_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%%.et_pb_blog_grid .et_pb_post',
                        'declaration' => sprintf('background-color: %1$s;', esc_html($masonry_tile_background_color))
                    ));
                }

                if ('' !== $overlay_icon_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay:before',
                        'declaration' => sprintf('color: %1$s !important;', esc_html($overlay_icon_color))
                    ));
                }

                if ('' !== $hover_overlay_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay',
                        'declaration' => sprintf('background-color: %1$s;', esc_html($hover_overlay_color))
                    ));
                }

                if ('on' === $use_overlay) {
                    $data_icon = '' !== $hover_icon ? sprintf(' data-icon="%1$s"', esc_attr(et_pb_process_font_icon($hover_icon))) : '';

                    $overlay_output = sprintf('<span class="et_overlay%1$s"%2$s></span>', ('' !== $hover_icon ? ' et_pb_inline_icon' : ''), $data_icon);
                }

                $overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

                if ('on' !== $fullwidth) {
                    if ('on' === $use_dropshadow) {
                        $module_class .= ' et_pb_blog_grid_dropshadow';
                    }

                    wp_enqueue_script('salvattore');

                    $background_layout = 'light';
                }

                $args = array(
                    'posts_per_page' => (int) $posts_number
                );

                $et_paged = is_front_page() ? get_query_var('page') : get_query_var('paged');

                if (is_front_page()) {
                    $paged = $et_paged;
                }

                if ('' !== $include_categories) {
                    $args['cat'] = $include_categories;
                }

                if (!is_search()) {
                    $args['paged'] = $et_paged;
                }

                if ('' !== $offset_number && !empty($offset_number)) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ($paged > 1) {
                        $args['offset'] = (($et_paged - 1) * intval($posts_number)) + intval($offset_number);
                    } else {
                        $args['offset'] = intval($offset_number);
                    }
                }

                if (is_single() && !isset($args['post__not_in'])) {
                    $args['post__not_in'] = array(
                        get_the_ID()
                    );
                }

                $args['meta_query'][] = array(
                    'key'     => '_EventStartDate',
                    'value'   => date('Y-m-d H:i:s'),
                    'compare' => '>',
                );

                $args['orderby'] = 'meta_value_num';
                $args['meta_key'] = '_EventStartDate';
                $args['order'] = 'ASC';

                $args['post_type'] = 'tribe_events';

                if (is_singular('nodo')) {
                    $post_id = get_the_ID();
                    $args['meta_query'][] = array(
                        'key'     => 'nodo',
                        'value'   => $post_id,
                        'compare' => '=',
                    );
                }

                ob_start();

                $events = new WP_Query($args);

                if ($events->have_posts()) {
                    global $post;
                    while ($events->have_posts()) {
                        $events->the_post();

                        $post_format = et_pb_post_format();

                        $thumb = '';

                        $width = 'on' === $fullwidth ? 1080 : 400;
                        $width = (int) apply_filters('et_pb_blog_image_width', $width);

                        $height    = 'on' === $fullwidth ? 675 : 250;
                        $height    = (int) apply_filters('et_pb_blog_image_height', $height);
                        $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
                        $titletext = get_the_title();
                        $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                        $thumb     = $thumbnail["thumb"];

                        $no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

                        if (in_array($post_format, array(
                            'video',
                            'gallery'
                        ))) {
                            $no_thumb_class = '';
                        } ?>

        	<article id="post-<?php
                        the_ID(); ?>" <?php
                        post_class('et_pb_post clearfix' . $no_thumb_class . $overlay_class); ?>>

        	<?php
                        et_divi_post_format_content();

                        if (!in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            if ('video' === $post_format && false !== ($first_video = et_get_first_video())):
                                $video_overlay = has_post_thumbnail() ? sprintf('<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
        	<div class="et_pb_video_overlay_hover">
        	<a href="#" class="et_pb_video_play"></a>
        	</div>
        	</div>', $thumb) : '';
                            printf('<div class="et_main_video_container">
        	%1$s
        	%2$s
        	</div>', $video_overlay, $first_video); elseif ('gallery' === $post_format):
                                et_pb_gallery_images('slider'); elseif ('' !== $thumb && 'on' === $show_thumbnail):
                                if ('on' !== $fullwidth) {
                                    echo '<div class="et_pb_image_container">';
                                } ?>
        	<a href="<?php
                                esc_url(the_permalink()); ?>" class="entry-featured-image-url">
        	<?php
                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
        	<?php
                                if ('on' === $use_overlay) {
                                    echo $overlay_output;
                                } ?>
        	</a>
        	<?php
                                if ('on' !== $fullwidth) {
                                    echo '</div> <!-- .et_pb_image_container -->';
                                }
                            endif;
                        } ?>

        	<?php
                        if ('off' === $fullwidth || !in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            ?>
        	<?php
                            if (!in_array($post_format, array(
                                'link',
                                'audio'
                            ))) {
                                ?>
        	<h2 class="entry-title"><a href="<?php
                                esc_url(the_permalink()); ?>"><?php
                                the_title(); ?></a></h2>
        	<?php

                            } ?>

        	<?php
                    //Fecha del evento
                    $date_start = new DateTime(get_post_meta($post->ID, '_EventStartDate', true));

                            if ('on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments) {
                                printf('<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>', ('on' === $show_author ? et_get_safe_localization(sprintf(__('by %s', 'et_builder'), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>')) : ''), (('on' === $show_author && 'on' === $show_date) ? ' | ' : ''), ('on' === $show_date ? et_get_safe_localization(sprintf(__('%s', 'et_builder'), '<span class="published">' . esc_html(date_i18n($meta_date, $date_start->format('U'))) . '</span>')) : ''), ((('on' === $show_author || 'on' === $show_date) && 'on' === $show_categories) ? ' | ' : ''), ('on' === $show_categories ? get_the_category_list(', ') : ''), ((('on' === $show_author || 'on' === $show_date || 'on' === $show_categories) && 'on' === $show_comments) ? ' | ' : ''), ('on' === $show_comments ? sprintf(esc_html(_nx('1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder')), number_format_i18n(get_comments_number())) : ''));
                            }

                            echo '<div class="post-content">';
                            global $et_pb_rendering_column_content;

                            $post_content = et_strip_shortcodes(et_delete_post_first_video(get_the_content()), true);

                            $et_pb_rendering_column_content = true;

                            if ('on' === $show_content) {
                                global $more;

                                // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                if (et_pb_is_pagebuilder_used(get_the_ID())) {
                                    $more = 1;
                                    echo apply_filters('the_content', $post_content);
                                } else {
                                    $more = null;
                                    echo apply_filters('the_content', et_delete_post_first_video(get_the_content(esc_html__('read more...', 'et_builder'))));
                                }
                            } else {
                                if (has_excerpt()) {
                                    the_excerpt();
                                } else {
                                    echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));
                                }
                            }

                            $et_pb_rendering_column_content = false;

                            if ('on' !== $show_content) {
                                $more = 'on' == $show_more ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
                                echo $more;
                            }

                            echo '</div>'; ?>
        	<?php

                        } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery'
?>

        	</article> <!-- .et_pb_post -->
        	<?php

                    } // endwhile

                    if ('on' === $show_pagination && !is_search()) {
                        echo '</div> <!-- .et_pb_posts -->';

                        $container_is_closed = true;

                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi();
                        } else {
                            if (et_is_builder_plugin_active()) {
                                include(ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php');
                            } else {
                                get_template_part('includes/navigation', 'index');
                            }
                        }
                    }

                    wp_reset_query();
                } else {
                    if (et_is_builder_plugin_active()) {
                        include(ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php');
                    } else {
                        get_template_part('includes/no-results', 'index');
                    }
                }

                $posts = ob_get_contents();

                ob_end_clean();

                $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

                $output = sprintf('<div%5$s class="%1$s%3$s%6$s"%7$s>
        	%2$s
        	%4$s', ('on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix'), $posts, esc_attr($class), (!$container_is_closed ? '</div> <!-- .et_pb_posts -->' : ''), ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''), ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''), ('on' !== $fullwidth ? ' data-columns' : ''));

                if ('on' !== $fullwidth) {
                    $output = sprintf('<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output);
                }

                // Restore $wp_filter
                $wp_filter = $wp_filter_cache;
                unset($wp_filter_cache);

                return $output;
            }
        }
        new ET_Builder_Module_Eventos;

        class ET_Builder_Module_Loop extends ET_Builder_Module
        {
            public function init()
            {
                $this->name       = esc_html__('Loop', 'et_builder');
                $this->slug       = 'et_pb_loop';
                $this->fb_support = true;

                $this->whitelisted_fields = array(
                    'fullwidth',
                    'posts_number',
                    'include_categories',
                    'meta_date',
                    'show_thumbnail',
                    'show_content',
                    'show_more',
                    'show_author',
                    'show_date',
                    'show_categories',
                    'show_comments',
                    'show_pagination',
                    'offset_number',
                    'background_layout',
                    'admin_label',
                    'module_id',
                    'module_class',
                    'masonry_tile_background_color',
                    'use_dropshadow',
                    'use_overlay',
                    'overlay_icon_color',
                    'hover_overlay_color',
                    'hover_icon'
                );

                $this->fields_defaults = array(
                    'fullwidth' => array(
                        'on'
                    ),
                    'posts_number' => array(
                        10,
                        'add_default_setting'
                    ),
                    'meta_date' => array(
                        'M j, Y',
                        'add_default_setting'
                    ),
                    'show_thumbnail' => array(
                        'on'
                    ),
                    'show_content' => array(
                        'off'
                    ),
                    'show_more' => array(
                        'off'
                    ),
                    'show_author' => array(
                        'on'
                    ),
                    'show_date' => array(
                        'on'
                    ),
                    'show_categories' => array(
                        'on'
                    ),
                    'show_comments' => array(
                        'off'
                    ),
                    'show_pagination' => array(
                        'on'
                    ),
                    'offset_number' => array(
                        0,
                        'only_default_setting'
                    ),
                    'background_layout' => array(
                        'light'
                    ),
                    'use_dropshadow' => array(
                        'off'
                    ),
                    'use_overlay' => array(
                        'off'
                    )
                );

                $this->main_css_element   = '%%order_class%% .et_pb_post';
                $this->advanced_options   = array(
                    'fonts' => array(
                        'header' => array(
                            'label' => esc_html__('Header', 'et_builder'),
                            'css' => array(
                                'main' => "{$this->main_css_element} .entry-title",
                                'plugin_main' => "{$this->main_css_element} .entry-title, {$this->main_css_element} .entry-title a",
                                'important' => 'all'
                            )
                        ),
                        'meta' => array(
                            'label' => esc_html__('Meta', 'et_builder'),
                            'css' => array(
                                'main' => "{$this->main_css_element} .post-meta"
                            )
                        ),
                        'body' => array(
                            'label' => esc_html__('Body', 'et_builder'),
                            'css' => array(
                                'color' => "{$this->main_css_element}, {$this->main_css_element} .post-content *",
                                'line_height' => "{$this->main_css_element} p"
                            )
                        )
                    ),
                    'border' => array(
                        'css' => array(
                            'main' => "%%order_class%%.et_pb_module .et_pb_post",
                            'important' => 'plugin_only'
                        )
                    )
                );
                $this->custom_css_options = array(
                    'title' => array(
                        'label' => esc_html__('Title', 'et_builder'),
                        'selector' => '.et_pb_post .entry-title'
                    ),
                    'post_meta' => array(
                        'label' => esc_html__('Post Meta', 'et_builder'),
                        'selector' => '.et_pb_post .post-meta'
                    ),
                    'pagenavi' => array(
                        'label' => esc_html__('Pagenavi', 'et_builder'),
                        'selector' => '.wp_pagenavi'
                    ),
                    'featured_image' => array(
                        'label' => esc_html__('Featured Image', 'et_builder'),
                        'selector' => '.et_pb_image_container'
                    ),
                    'read_more' => array(
                        'label' => esc_html__('Read More Button', 'et_builder'),
                        'selector' => '.et_pb_post .more-link'
                    )
                );
            }

            public function get_fields()
            {
                $fields = array(
                    'fullwidth' => array(
                        'label' => esc_html__('Layout', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'layout',
                        'options' => array(
                            'on' => esc_html__('Fullwidth', 'et_builder'),
                            'off' => esc_html__('Grid', 'et_builder')
                        ),
                        'affects' => array(
                            'background_layout',
                            'use_dropshadow',
                            'masonry_tile_background_color'
                        ),
                        'description' => esc_html__('Toggle between the various blog layout types.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'posts_number' => array(
                        'label' => esc_html__('Posts Number', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('Choose how much posts you would like to display per page.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'include_categories' => array(
                        'label' => esc_html__('Include Categories', 'et_builder'),
                        'renderer' => 'et_builder_include_categories_option',
                        'option_category' => 'basic_option',
                        'renderer_options' => array(
                            'use_terms' => false
                        ),
                        'description' => esc_html__('Choose which categories you would like to include in the feed.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'meta_date' => array(
                        'label' => esc_html__('Meta Date Format', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_thumbnail' => array(
                        'label' => esc_html__('Show Featured Image', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('This will turn thumbnails on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_content' => array(
                        'label' => esc_html__('Content', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'configuration',
                        'options' => array(
                            'off' => esc_html__('Show Excerpt', 'et_builder'),
                            'on' => esc_html__('Show Content', 'et_builder')
                        ),
                        'affects' => array(
                            'show_more'
                        ),
                        'description' => esc_html__('Showing the full content will not truncate your posts on the index page. Showing the excerpt will only display your excerpt text.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_more' => array(
                        'label' => esc_html__('Read More Button', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'depends_show_if' => 'off',
                        'description' => esc_html__('Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_author' => array(
                        'label' => esc_html__('Show Author', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn on or off the author link.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_date' => array(
                        'label' => esc_html__('Show Date', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn the date on or off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_categories' => array(
                        'label' => esc_html__('Show Categories', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn the category links on or off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_comments' => array(
                        'label' => esc_html__('Show Comment Count', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn comment count on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'show_pagination' => array(
                        'label' => esc_html__('Show Pagination', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'configuration',
                        'options' => array(
                            'on' => esc_html__('Yes', 'et_builder'),
                            'off' => esc_html__('No', 'et_builder')
                        ),
                        'description' => esc_html__('Turn pagination on and off.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'offset_number' => array(
                        'label' => esc_html__('Offset Number', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'description' => esc_html__('Choose how many posts you would like to offset by', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'use_overlay' => array(
                        'label' => esc_html__('Featured Image Overlay', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'layout',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'affects' => array(
                            'overlay_icon_color',
                            'hover_overlay_color',
                            'hover_icon'
                        ),
                        'description' => esc_html__('If enabled, an overlay color and icon will be displayed when a visitors hovers over the featured image of a post.', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'overlay_icon_color' => array(
                        'label' => esc_html__('Overlay Icon Color', 'et_builder'),
                        'type' => 'color',
                        'custom_color' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom color for the overlay icon', 'et_builder')
                    ),
                    'hover_overlay_color' => array(
                        'label' => esc_html__('Hover Overlay Color', 'et_builder'),
                        'type' => 'color-alpha',
                        'custom_color' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom color for the overlay', 'et_builder')
                    ),
                    'hover_icon' => array(
                        'label' => esc_html__('Hover Icon Picker', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'class' => array(
                            'et-pb-font-icon'
                        ),
                        'renderer' => 'et_pb_get_font_icon_list',
                        'renderer_with_field' => true,
                        'depends_show_if' => 'on',
                        'description' => esc_html__('Here you can define a custom icon for the overlay', 'et_builder'),
                        'computed_affects' => array(
                            '__posts'
                        )
                    ),
                    'background_layout' => array(
                        'label' => esc_html__('Text Color', 'et_builder'),
                        'type' => 'select',
                        'option_category' => 'color_option',
                        'options' => array(
                            'light' => esc_html__('Dark', 'et_builder'),
                            'dark' => esc_html__('Light', 'et_builder')
                        ),
                        'depends_default' => true,
                        'description' => esc_html__('Here you can choose whether your text should be light or dark. If you are working with a dark background, then your text should be light. If your background is light, then your text should be set to dark.', 'et_builder')
                    ),
                    'masonry_tile_background_color' => array(
                        'label' => esc_html__('Grid Tile Background Color', 'et_builder'),
                        'type' => 'color-alpha',
                        'custom_color' => true,
                        'tab_slug' => 'advanced',
                        'depends_show_if' => 'off',
                        'depends_to' => array(
                            'fullwidth'
                        )
                    ),
                    'use_dropshadow' => array(
                        'label' => esc_html__('Use Dropshadow', 'et_builder'),
                        'type' => 'yes_no_button',
                        'option_category' => 'layout',
                        'options' => array(
                            'off' => esc_html__('Off', 'et_builder'),
                            'on' => esc_html__('On', 'et_builder')
                        ),
                        'tab_slug' => 'advanced',
                        'depends_show_if' => 'off',
                        'depends_to' => array(
                            'fullwidth'
                        )
                    ),
                    'disabled_on' => array(
                        'label' => esc_html__('Disable on', 'et_builder'),
                        'type' => 'multiple_checkboxes',
                        'options' => array(
                            'phone' => esc_html__('Phone', 'et_builder'),
                            'tablet' => esc_html__('Tablet', 'et_builder'),
                            'desktop' => esc_html__('Desktop', 'et_builder')
                        ),
                        'additional_att' => 'disable_on',
                        'option_category' => 'configuration',
                        'description' => esc_html__('This will disable the module on selected devices', 'et_builder')
                    ),
                    'admin_label' => array(
                        'label' => esc_html__('Admin Label', 'et_builder'),
                        'type' => 'text',
                        'description' => esc_html__('This will change the label of the module in the builder for easy identification.', 'et_builder')
                    ),
                    'module_id' => array(
                        'label' => esc_html__('CSS ID', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'tab_slug' => 'custom_css',
                        'option_class' => 'et_pb_custom_css_regular'
                    ),
                    'module_class' => array(
                        'label' => esc_html__('CSS Class', 'et_builder'),
                        'type' => 'text',
                        'option_category' => 'configuration',
                        'tab_slug' => 'custom_css',
                        'option_class' => 'et_pb_custom_css_regular'
                    ),
                    '__posts' => array(
                        'type' => 'computed',
                        'computed_callback' => array(
                            'ET_Builder_Module_Blog',
                            'get_blog_posts'
                        ),
                        'computed_depends_on' => array(
                            'fullwidth',
                            'posts_number',
                            'include_categories',
                            'meta_date',
                            'show_thumbnail',
                            'show_content',
                            'show_more',
                            'show_author',
                            'show_date',
                            'show_categories',
                            'show_comments',
                            'show_pagination',
                            'offset_number',
                            'use_overlay',
                            'hover_icon'
                        )
                    )
                );
                return $fields;
            }

            /**
             * Get blog posts for blog module
             *
             * @param array   arguments that is being used by et_pb_blog
             * @return string blog post markup
             */
            public static function get_blog_posts($args = array(), $conditional_tags = array(), $current_page = array())
            {
                global $paged, $post, $wp_query, $et_fb_processing_shortcode_object, $et_pb_rendering_column_content;

                $global_processing_original_value = $et_fb_processing_shortcode_object;

                // Default params are combination of attributes that is used by et_pb_blog and
                // conditional tags that need to be simulated (due to AJAX nature) by passing args
                $defaults = array(
                    'fullwidth' => '',
                    'posts_number' => '',
                    'include_categories' => '',
                    'meta_date' => '',
                    'show_thumbnail' => '',
                    'show_content' => '',
                    'show_author' => '',
                    'show_date' => '',
                    'show_categories' => '',
                    'show_comments' => '',
                    'show_pagination' => '',
                    'background_layout' => '',
                    'show_more' => '',
                    'offset_number' => '',
                    'masonry_tile_background_color' => '',
                    'use_dropshadow' => '',
                    'overlay_icon_color' => '',
                    'hover_overlay_color' => '',
                    'hover_icon' => '',
                    'use_overlay' => ''
                );

                // WordPress' native conditional tag is only available during page load. It'll fail during component update because
                // et_pb_process_computed_property() is loaded in admin-ajax.php. Thus, use WordPress' conditional tags on page load and
                // rely to passed $conditional_tags for AJAX call
                $is_front_page               = et_fb_conditional_tag('is_front_page', $conditional_tags);
                $is_search                   = et_fb_conditional_tag('is_search', $conditional_tags);
                $is_single                   = et_fb_conditional_tag('is_single', $conditional_tags);
                $et_is_builder_plugin_active = et_fb_conditional_tag('et_is_builder_plugin_active', $conditional_tags);

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters('wp_audio_shortcode_library');
                remove_all_filters('wp_audio_shortcode');
                remove_all_filters('wp_audio_shortcode_class');

                $args = wp_parse_args($args, $defaults);

                $overlay_output = '';
                $hover_icon     = '';

                if ('on' === $args['use_overlay']) {
                    $data_icon = '' !== $args['hover_icon'] ? sprintf(' data-icon="%1$s"', esc_attr(et_pb_process_font_icon($args['hover_icon']))) : '';

                    $overlay_output = sprintf('<span class="et_overlay%1$s"%2$s></span>', ('' !== $args['hover_icon'] ? ' et_pb_inline_icon' : ''), $data_icon);
                }

                $overlay_class = 'on' === $args['use_overlay'] ? ' et_pb_has_overlay' : '';

                $query_args = array(
                    'posts_per_page' => intval($args['posts_number']),
                    'post_status' => 'publish'
                );

                if (defined('DOING_AJAX') && isset($current_page['paged'])) {
                    $paged = intval($current_page['paged']);
                } else {
                    $paged = $is_front_page ? get_query_var('page') : get_query_var('paged');
                }

                if ('' !== $args['include_categories']) {
                    $query_args['cat'] = $args['include_categories'];
                }

                if (!$is_search) {
                    $query_args['paged'] = $paged;
                }

                if ('' !== $args['offset_number'] && !empty($args['offset_number'])) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ($paged > 1) {
                        $query_args['offset'] = (($paged - 1) * intval($args['posts_number'])) + intval($args['offset_number']);
                    } else {
                        $query_args['offset'] = intval($args['offset_number']);
                    }

                    /**
                     * If no category selected and it is ajax request, the offset starts from zero instead of one. Adjust accordingly.
                     */
                    if (!isset($query_args['cat']) && defined('DOING_AJAX')) {
                        $query_args['offset'] = intval($query_args['offset']) + 1;
                    }
                }

                if ($is_single) {
                    $query_args['post__not_in'][] = get_the_ID();
                }

                ob_start();

                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        global $et_fb_processing_shortcode_object;

                        $global_processing_original_value = $et_fb_processing_shortcode_object;

                        // reset the fb processing flag
                        $et_fb_processing_shortcode_object = false;

                        $thumb          = '';
                        $width          = 'on' === $args['fullwidth'] ? 1080 : 400;
                        $width          = (int) apply_filters('et_pb_blog_image_width', $width);
                        $height         = 'on' === $args['fullwidth'] ? 675 : 250;
                        $height         = (int) apply_filters('et_pb_blog_image_height', $height);
                        $classtext      = 'on' === $args['fullwidth'] ? 'et_pb_post_main_image' : '';
                        $titletext      = get_the_title();
                        $thumbnail      = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                        $thumb          = $thumbnail["thumb"];
                        $no_thumb_class = '' === $thumb || 'off' === $args['show_thumbnail'] ? ' et_pb_no_thumb' : '';

                        $post_format = et_pb_post_format();
                        if (in_array($post_format, array(
                            'video',
                            'gallery'
                        ))) {
                            $no_thumb_class = '';
                        }

                        // Print output
?>
        	<article id="" <?php
                        post_class('et_pb_post clearfix' . $no_thumb_class . $overlay_class); ?>>
        	<?php
                        et_divi_post_format_content();

                        if (!in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            if ('video' === $post_format && false !== ($first_video = et_get_first_video())):
                                $video_overlay = has_post_thumbnail() ? sprintf('<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
        	<div class="et_pb_video_overlay_hover">
        	<a href="#" class="et_pb_video_play"></a>
        	</div>
        	</div>', $thumb) : '';
                            printf('<div class="et_main_video_container">
        	%1$s
        	%2$s
        	</div>', $video_overlay, $first_video); elseif ('gallery' === $post_format):
                                et_pb_gallery_images('slider'); elseif ('' !== $thumb && 'on' === $args['show_thumbnail']):
                                if ('on' !== $args['fullwidth']) {
                                    echo '<div class="et_pb_image_container">';
                                } ?>
        	<a href="<?php
                                esc_url(the_permalink()); ?>" class="entry-featured-image-url">
        	<?php
                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
        	<?php
                                if ('on' === $args['use_overlay']) {
                                    echo $overlay_output;
                                } ?>
        	</a>
        	<?php
                                if ('on' !== $args['fullwidth']) {
                                    echo '</div> <!-- .et_pb_image_container -->';
                                }
                            endif;
                        } ?>

        	<?php
                        if ('off' === $args['fullwidth'] || !in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            ?>
        	<?php
                            if (!in_array($post_format, array(
                                'link',
                                'audio'
                            ))) {
                                ?>
        	<h2 class="entry-title"><a href="<?php
                                esc_url(the_permalink()); ?>"><?php
                                the_title(); ?></a></h2>
        	<?php

                            } ?>

        	<?php
                            if ('on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories'] || 'on' === $args['show_comments']) {
                                printf('<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>', ('on' === $args['show_author'] ? et_get_safe_localization(sprintf(__('by %s', 'et_builder'), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>')) : ''), (('on' === $args['show_author'] && 'on' === $args['show_date']) ? ' | ' : ''), ('on' === $args['show_date'] ? et_get_safe_localization(sprintf(__('%s', 'et_builder'), '<span class="published">' . esc_html(get_the_date($args['meta_date'])) . '</span>')) : ''), ((('on' === $args['show_author'] || 'on' === $args['show_date']) && 'on' === $args['show_categories']) ? ' | ' : ''), ('on' === $args['show_categories'] ? get_the_category_list(', ') : ''), ((('on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories']) && 'on' === $args['show_comments']) ? ' | ' : ''), ('on' === $args['show_comments'] ? sprintf(esc_html(_nx('1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder')), number_format_i18n(get_comments_number())) : ''));
                            }

                            $post_content = et_strip_shortcodes(et_delete_post_first_video(get_the_content()), true);

                            // reset the fb processing flag
                            $et_fb_processing_shortcode_object = false;
                            // set the flag to indicate that we're processing internal content
                            $et_pb_rendering_column_content    = true;
                            // reset all the attributes required to properly generate the internal styles
                            ET_Builder_Element::clean_internal_modules_styles();

                            echo '<div class="post-content">';

                            if ('on' === $args['show_content']) {
                                global $more;

                                // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                if (et_pb_is_pagebuilder_used(get_the_ID())) {
                                    $more = 1;

                                    echo apply_filters('the_content', $post_content);
                                } else {
                                    $more = null;
                                    echo apply_filters('the_content', et_delete_post_first_video(get_the_content(esc_html__('read more...', 'et_builder'))));
                                }
                            } else {
                                if (has_excerpt()) {
                                    the_excerpt();
                                } else {
                                    if ('' !== $post_content) {
                                        // set the $et_fb_processing_shortcode_object to false, to retrieve the content inside truncate_post() correctly
                                        $et_fb_processing_shortcode_object = false;
                                        echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));
                                        // reset the $et_fb_processing_shortcode_object to its original value
                                        $et_fb_processing_shortcode_object = $global_processing_original_value;
                                    } else {
                                        echo '';
                                    }
                                }
                            }

                            $et_fb_processing_shortcode_object = $global_processing_original_value;
                            // retrieve the styles for the modules inside Blog content
                            $internal_style                    = ET_Builder_Element::get_style(true);
                            // reset all the attributes after we retrieved styles
                            ET_Builder_Element::clean_internal_modules_styles(false);
                            $et_pb_rendering_column_content = false;
                            // append styles to the blog content
                            if ($internal_style) {
                                printf('<style type="text/css" class="et_fb_blog_inner_content_styles">
        	%1$s
        	</style>', $internal_style);
                            }

                            echo '</div>';

                            if ('on' !== $args['show_content']) {
                                $more = 'on' == $args['show_more'] ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
                                echo $more;
                            } ?>
        	<?php

                        } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery'
?>
        	</article>
        	<?php

                        $et_fb_processing_shortcode_object = $global_processing_original_value;
                    } // endwhile

                    if ('on' === $args['show_pagination'] && !$is_search) {
                        // echo '</div> <!-- .et_pb_posts -->'; // @todo this causes closing tag issue

                        $container_is_closed = true;

                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi(array(
                                'query' => $query
                            ));
                        } else {
                            if ($et_is_builder_plugin_active) {
                                include(ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php');
                            } else {
                                get_template_part('includes/navigation', 'index');
                            }
                        }
                    }

                    wp_reset_query();
                } else {
                    if ($et_is_builder_plugin_active) {
                        include(ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php');
                    } else {
                        get_template_part('includes/no-results', 'index');
                    }
                }

                wp_reset_postdata();

                // Reset $wp_query to its origin
                $wp_query = $wp_query_page;

                $posts = ob_get_contents();

                ob_end_clean();

                return $posts;
            }

            public function shortcode_callback($atts, $content = null, $function_name)
            {
                /**
                 * Cached $wp_filter so it can be restored at the end of the callback.
                 * This is needed because this callback uses the_content filter / calls a function
                 * which uses the_content filter. WordPress doesn't support nested filter
                 */
                global $wp_filter;
                $wp_filter_cache = $wp_filter;

                $module_id                     = $this->shortcode_atts['module_id'];
                $module_class                  = $this->shortcode_atts['module_class'];
                $fullwidth                     = $this->shortcode_atts['fullwidth'];
                $posts_number                  = $this->shortcode_atts['posts_number'];
                $include_categories            = $this->shortcode_atts['include_categories'];
                $meta_date                     = $this->shortcode_atts['meta_date'];
                $show_thumbnail                = $this->shortcode_atts['show_thumbnail'];
                $show_content                  = $this->shortcode_atts['show_content'];
                $show_author                   = $this->shortcode_atts['show_author'];
                $show_date                     = $this->shortcode_atts['show_date'];
                $show_categories               = $this->shortcode_atts['show_categories'];
                $show_comments                 = $this->shortcode_atts['show_comments'];
                $show_pagination               = $this->shortcode_atts['show_pagination'];
                $background_layout             = $this->shortcode_atts['background_layout'];
                $show_more                     = $this->shortcode_atts['show_more'];
                $offset_number                 = $this->shortcode_atts['offset_number'];
                $masonry_tile_background_color = $this->shortcode_atts['masonry_tile_background_color'];
                $use_dropshadow                = $this->shortcode_atts['use_dropshadow'];
                $overlay_icon_color            = $this->shortcode_atts['overlay_icon_color'];
                $hover_overlay_color           = $this->shortcode_atts['hover_overlay_color'];
                $hover_icon                    = $this->shortcode_atts['hover_icon'];
                $use_overlay                   = $this->shortcode_atts['use_overlay'];

                global $paged;

                $module_class = ET_Builder_Element::add_module_order_class($module_class, $function_name);

                $container_is_closed = false;

                // remove all filters from WP audio shortcode to make sure current theme doesn't add any elements into audio module
                remove_all_filters('wp_audio_shortcode_library');
                remove_all_filters('wp_audio_shortcode');
                remove_all_filters('wp_audio_shortcode_class');

                if ('' !== $masonry_tile_background_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%%.et_pb_blog_grid .et_pb_post',
                        'declaration' => sprintf('background-color: %1$s;', esc_html($masonry_tile_background_color))
                    ));
                }

                if ('' !== $overlay_icon_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay:before',
                        'declaration' => sprintf('color: %1$s !important;', esc_html($overlay_icon_color))
                    ));
                }

                if ('' !== $hover_overlay_color) {
                    ET_Builder_Element::set_style($function_name, array(
                        'selector' => '%%order_class%% .et_overlay',
                        'declaration' => sprintf('background-color: %1$s;', esc_html($hover_overlay_color))
                    ));
                }

                if ('on' === $use_overlay) {
                    $data_icon = '' !== $hover_icon ? sprintf(' data-icon="%1$s"', esc_attr(et_pb_process_font_icon($hover_icon))) : '';

                    $overlay_output = sprintf('<span class="et_overlay%1$s"%2$s></span>', ('' !== $hover_icon ? ' et_pb_inline_icon' : ''), $data_icon);
                }

                $overlay_class = 'on' === $use_overlay ? ' et_pb_has_overlay' : '';

                if ('on' !== $fullwidth) {
                    if ('on' === $use_dropshadow) {
                        $module_class .= ' et_pb_blog_grid_dropshadow';
                    }

                    wp_enqueue_script('salvattore');

                    $background_layout = 'light';
                }

                $args = array(
                    'posts_per_page' => (int) $posts_number
                );

                $et_paged = is_front_page() ? get_query_var('page') : get_query_var('paged');

                if (is_front_page()) {
                    $paged = $et_paged;
                }

                if ('' !== $include_categories) {
                    $args['cat'] = $include_categories;
                }

                if (!is_search()) {
                    $args['paged'] = $et_paged;
                }

                if ('' !== $offset_number && !empty($offset_number)) {
                    /**
                     * Offset + pagination don't play well. Manual offset calculation required
                     * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
                     */
                    if ($paged > 1) {
                        $args['offset'] = (($et_paged - 1) * intval($posts_number)) + intval($offset_number);
                    } else {
                        $args['offset'] = intval($offset_number);
                    }
                }

                if (is_single() && !isset($args['post__not_in'])) {
                    $args['post__not_in'] = array(
                        get_the_ID()
                    );
                }

                $args['meta_query'][] = array(
                    'key'     => '_EventStartDate',
                    'value'   => date('Y-m-d H:i:s'),
                    'compare' => '>',
                );

                ob_start();

                if (have_posts()) {
                    global $post;
                    while (have_posts()) {
                        the_post();

                        $post_format = et_pb_post_format();

                        $thumb = '';

                        $width = 'on' === $fullwidth ? 1080 : 400;
                        $width = (int) apply_filters('et_pb_blog_image_width', $width);

                        $height    = 'on' === $fullwidth ? 675 : 250;
                        $height    = (int) apply_filters('et_pb_blog_image_height', $height);
                        $classtext = 'on' === $fullwidth ? 'et_pb_post_main_image' : '';
                        $titletext = get_the_title();
                        $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                        $thumb     = $thumbnail["thumb"];

                        $no_thumb_class = '' === $thumb || 'off' === $show_thumbnail ? ' et_pb_no_thumb' : '';

                        if (in_array($post_format, array(
                            'video',
                            'gallery'
                        ))) {
                            $no_thumb_class = '';
                        } ?>

        	<article id="post-<?php
                        the_ID(); ?>" <?php
                        post_class('et_pb_post clearfix' . $no_thumb_class . $overlay_class); ?>>

        	<?php
                        et_divi_post_format_content();

                        if (!in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            if ('video' === $post_format && false !== ($first_video = et_get_first_video())):
                                $video_overlay = has_post_thumbnail() ? sprintf('<div class="et_pb_video_overlay" style="background-image: url(%1$s); background-size: cover;">
        	<div class="et_pb_video_overlay_hover">
        	<a href="#" class="et_pb_video_play"></a>
        	</div>
        	</div>', $thumb) : '';
                            printf('<div class="et_main_video_container">
        	%1$s
        	%2$s
        	</div>', $video_overlay, $first_video); elseif ('gallery' === $post_format):
                                et_pb_gallery_images('slider'); elseif ('' !== $thumb && 'on' === $show_thumbnail):
                                if ('on' !== $fullwidth) {
                                    echo '<div class="et_pb_image_container">';
                                } ?>
        	<a href="<?php
                                esc_url(the_permalink()); ?>" class="entry-featured-image-url">
        	<?php
                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height); ?>
        	<?php
                                if ('on' === $use_overlay) {
                                    echo $overlay_output;
                                } ?>
        	</a>
        	<?php
                                if ('on' !== $fullwidth) {
                                    echo '</div> <!-- .et_pb_image_container -->';
                                }
                            endif;
                        } ?>

        	<?php
                        if ('off' === $fullwidth || !in_array($post_format, array(
                            'link',
                            'audio',
                            'quote'
                        ))) {
                            ?>
        	<?php
                            if (!in_array($post_format, array(
                                'link',
                                'audio'
                            ))) {
                                ?>
        	<h2 class="entry-title"><a href="<?php
                                esc_url(the_permalink()); ?>"><?php
                                the_title(); ?></a></h2>
        	<?php

                            } ?>

        	<?php
                    //Fecha del evento
                    $date_start = new DateTime(get_post_meta($post->ID, '_EventStartDate', true));

                            if ('on' === $show_author || 'on' === $show_date || 'on' === $show_categories || 'on' === $show_comments) {
                                printf('<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>', ('on' === $show_author ? et_get_safe_localization(sprintf(__('by %s', 'et_builder'), '<span class="author vcard">' . et_pb_get_the_author_posts_link() . '</span>')) : ''), (('on' === $show_author && 'on' === $show_date) ? ' | ' : ''), ('on' === $show_date ? et_get_safe_localization(sprintf(__('%s', 'et_builder'), '<span class="published">' . esc_html(date_i18n($meta_date, $date_start->format('U'))) . '</span>')) : ''), ((('on' === $show_author || 'on' === $show_date) && 'on' === $show_categories) ? ' | ' : ''), ('on' === $show_categories ? get_the_category_list(', ') : ''), ((('on' === $show_author || 'on' === $show_date || 'on' === $show_categories) && 'on' === $show_comments) ? ' | ' : ''), ('on' === $show_comments ? sprintf(esc_html(_nx('1 Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder')), number_format_i18n(get_comments_number())) : ''));
                            }

                            echo '<div class="post-content">';
                            global $et_pb_rendering_column_content;

                            $post_content = et_strip_shortcodes(et_delete_post_first_video(get_the_content()), true);

                            $et_pb_rendering_column_content = true;

                            if ('on' === $show_content) {
                                global $more;

                                // page builder doesn't support more tag, so display the_content() in case of post made with page builder
                                if (et_pb_is_pagebuilder_used(get_the_ID())) {
                                    $more = 1;
                                    echo apply_filters('the_content', $post_content);
                                } else {
                                    $more = null;
                                    echo apply_filters('the_content', et_delete_post_first_video(get_the_content(esc_html__('read more...', 'et_builder'))));
                                }
                            } else {
                                if (has_excerpt()) {
                                    the_excerpt();
                                } else {
                                    echo wpautop(et_delete_post_first_video(strip_shortcodes(truncate_post(270, false, '', true))));
                                }
                            }

                            $et_pb_rendering_column_content = false;

                            if ('on' !== $show_content) {
                                $more = 'on' == $show_more ? sprintf(' <a href="%1$s" class="more-link" >%2$s</a>', esc_url(get_permalink()), esc_html__('read more', 'et_builder')) : '';
                                echo $more;
                            }

                            echo '</div>'; ?>
        	<?php

                        } // 'off' === $fullwidth || ! in_array( $post_format, array( 'link', 'audio', 'quote', 'gallery'
?>

        	</article> <!-- .et_pb_post -->
        	<?php

                    } // endwhile

                    if ('on' === $show_pagination && !is_search()) {
                        echo '</div> <!-- .et_pb_posts -->';

                        $container_is_closed = true;

                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi();
                        } else {
                            if (et_is_builder_plugin_active()) {
                                include(ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php');
                            } else {
                                get_template_part('includes/navigation', 'index');
                            }
                        }
                    }

                    wp_reset_query();
                } else {
                    if (et_is_builder_plugin_active()) {
                        include(ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php');
                    } else {
                        get_template_part('includes/no-results', 'index');
                    }
                }

                $posts = ob_get_contents();

                ob_end_clean();

                $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

                $output = sprintf('<div%5$s class="%1$s%3$s%6$s"%7$s>
        	%2$s
        	%4$s', ('on' === $fullwidth ? 'et_pb_posts' : 'et_pb_blog_grid clearfix'), $posts, esc_attr($class), (!$container_is_closed ? '</div> <!-- .et_pb_posts -->' : ''), ('' !== $module_id ? sprintf(' id="%1$s"', esc_attr($module_id)) : ''), ('' !== $module_class ? sprintf(' %1$s', esc_attr($module_class)) : ''), ('on' !== $fullwidth ? ' data-columns' : ''));

                if ('on' !== $fullwidth) {
                    $output = sprintf('<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output);
                }

                // Restore $wp_filter
                $wp_filter = $wp_filter_cache;
                unset($wp_filter_cache);

                return $output;
            }
        }
        new ET_Builder_Module_Loop;
    }
}

add_action('et_builder_ready', 'gg_divi_modules');
?>
